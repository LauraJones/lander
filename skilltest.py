"""
Skill Tester: The 
Land rocket as fast and with as much fuel as possible
"""


import pygame
from pygame.locals import *
import os.path
import cmath
import math
from scipy.stats import linregress
import random
import time
import pygame_gui
import json
import baseplayer

class Rocket(baseplayer.baseplayer):
    """
    Rocket class is the player in he game
    draws a rocket, effected by gravity. Crashes if lands on terrain, or lands if lands on landing pad slowly enough
    """
    def __init__(self,options,settings,highscores,game):
        """
        Initalisation. saves parameters and sets initial ship state
        
        INPUTS:
        options: options (things like which image sprite to use, gravity, etc)
        settings: settings
        highscores: highscores object
        game: game object
        """
        super().__init__(options,settings,highscores,game)
        #save options
        #highscores
        self._highscores=highscores
        #and set up table
        self._highscores.creategametype(
            "skilltester",
            json.dumps(["name","score","numlands","datetime"]),
            json.dumps({"numlands":"# lands","datetime":"Date"})
            )
        #scoring factors
        self._scoringfactors=options["scoring"]
        #and current pad width and time limits
        self._landingpads=options["pads"]
        self._padwidth=self._landingpads["initpadwidth"]
        self.remainingtime=options["initialtime"] #HUD looks for this
        self._timedecrement=options["decrementperland"]
        self._timeincrement=self.remainingtime
        self._numlands=0
        self._blockcrashtime=0
        self._landcountdown=None

    
    def on_init(self,screen,x,y,manager):
        """
        Set up graphics, read image files etc
        
        INPUTS:
        screen: The screen to draw on
        x: Starting x location
        y: Starting y location
        manager: GUI manager for window
        
        OUTPUTS: 
        None
        """
        #init superclass
        super().on_init(screen,x,y,manager)
        #now bases (list of images)
        bases=self._terrainoptions["baseimages"]
        self._baseimgs=[]
        for base in bases:
            #path
            fpath=os.path.join("images",base)
            #load file
            self._baseimgs.append(pygame.image.load(fpath).convert_alpha())
        #show welcome
        self.showwelcome()

     
    def loop(self):
        """
        Loop. call main class and then check for land/crash
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        super().loop()
        self.landingpads()
        self.terrain()
        #and timer
        self.time()

    def time(self):
        """
        Calculate timer and victory conditions
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if not (self.paused or self.crashed):
            ct=time.time()
            self.remainingtime-=(ct-self._lasttime)
            self._lasttime=ct
            if self.remainingtime<=0:
                #say we crashed so that time stops, show win screen
                self.crashed=True
                #calc score and show
                self.score,self._scoredict=self.calculatescore(False)
                self._dlg=dialogue(
                    not self.crashed,self.score,self._scoredict,self._highscores,self._screen,self._manager,self.exit,"package"
                    )
                
        elif self.paused:
            #ensure time slips
            self._lasttime=time.time()
   
    def landingpads(self):
        """
        Assess landing pads to see if we have landed
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.crashed or (time.time()<self._blockcrashtime):
            return
        if self.rect.colliderect(self._landingpad.rect):
            #landed/crashed
            if self.safeland():
                self.land()
            else:
                self.angle=0
                self.crash()
            return
        else:
            self.landed=False
    
    def terrain(self):
        """
        Our overloaded version simply prevents crashing on map generation
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.crashed or (time.time()<self._blockcrashtime):
            return
        super().terrain()
    
    def land(self):
        """
        Land on a landing pad. Rebuild, terrain, start again.
        
        INPUTS:
        pady: the landing pad height
        
        OUTPUTS:
        None
        """
        if self.landed:
            if self._landcountdown is None:
                return
            if time.time()>self._landcountdown:
                #pause
                self.paused=True
                self.pausestatus()
                #num lands
                self._numlands+=1
                #and message
                desc="Congratulations! You have landed {} times so far".format(self._numlands)
                self._dlg=baseplayer.message(desc,"skilltester",self.newmap,self._screen,self._manager)
                self._landcountdown=None
                
            return
        #set landed flag
        self.landed=True
        
        
        #and set velocity, angle to zero
        self._vx=0
        self._vy=0
        self.angle=0
        
        #now we set a timer. We want 0.5 second of landing needed to be counted
        self._landcountdown=time.time()+.5
        
        
        
        
    def newmap(self):
        """
        New map callback. Called when the user dismisses the new map dialogue
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #new pad sizes
        self._padwidth-=self._landingpads["decrementperland"]
        self._padwidth=max([self._padwidth,self._landingpads["minpadwidth"]])
        
        #rebuild terrain        
        self._game.redraw_terrain(randomise=True)
        
        
        #and add fuel
        self.fuel+=self.initfuel*0.5
        self.fuel=min([self.fuel,self.initfuel])
        #and time
        self.remainingtime+=self._timeincrement
        self._timeincrement-=self._timedecrement
        
        #reset our location 
        self.coords=[self._settings["mapwidth"]/2,100]
        
        #unland
        self.landed=False
        
        #And unpause
        self.unpause()
        
        #and we block crashing for 100ms or so otherwise we immediately crash sometimes
        self._blockcrashtime=time.time()+0.1
        
    
    def calculatescore(self,crashed=True):
        """
        Calculate the score 
        
        INPUTS:
        None
        
        OUTPUTS:
        score, scoring parameters
        """
        if self._scored:
            #avoid recalcs
            return self.score,self._scoredict
            
        #scores are based on number of lands 
        score=self._scoringfactors["land"]*self._numlands
        if crashed:
            score=score*self._scoringfactors["crashmultiplier"]
        
        #now the factors
        #first autostraighten. this is simply boolean 
        sf=1
        if self._autostraighten:
            sf=self._scoringfactors["straighten"]
            score=score*sf
        #now for gravity. Gravity centres on 1 at centregravity
        #on the high end it reaches highgravityval at higgravity
        #on the low end it reaches lowgravityval at lowgravity
        if self._gravity>self._scoringfactors["centregravity"]:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["highgravity"]-self._scoringfactors["centregravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["highgravityval"]-1)+1
        else:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["centregravity"]-self._scoringfactors["lowgravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["lowgravityval"]-1)+1
        score=score*gfactor
        #now power/weight ratio
        #higher is obviously easier. We make it linear between 1 and 3, where 1 is powerweightmult and 5 is 1
        pwratio=self._thrusterforce/(self._mass*self._gravity)
        slope=-(self._scoringfactors["powerweightmult"]-1)/2
        pwratiofactor=max(
            self._scoringfactors["minpowerweightmult"],
            self._scoringfactors["powerweightmult"]+pwratio*slope
            )
        #save the params in a dict for later
        dct=dict(
            fuelrem=self.fuelpc,
            numlands=self._numlands,
            straighen=self._autostraighten,
            gravity=self._gravity,
            powerweight=pwratio,
            straightenfactor=sf,
            gravityfactor=gfactor,
            pwratiofactor=pwratiofactor
            )
        return score*pwratiofactor,dct
                

        
    def safeland(self):
        """
        Determine if a landing is safe or should result in a crash
        
        INPUTS:
        None
        
        OUTPUTS:
        bool (true for safe landing)
        """
        return math.sqrt(self._vx**2+self._vy**2)<=self._maxlandingvelocity
        
        
    def addlandingpads(self,terrain):
        """
        Add landing pads and related scenery to map
        Called immediately after hightmap is made
        
        INPUTS:
        terrain: terrain object
        
        OUTPUTS:
        heightmap and list of sprites
        """
        #save terrian object
        self._terrain=terrain
        #landing pad is placed at the flattest spot on the map
        padwidth=self._padwidth
        #first, create a landing pad
        landingpad_o=baseplayer.landingpad(3,padwidth,[0,255,0])
        #now find the best spot
        best,selector=terrain.findarea(padwidth,landingpad_o.suitabilitychecker)
        #lets not worry about None here, as there should be somwhwere
        #and place
        height=terrain.createfoundation(best,padwidth)
        landingpad_o.on_init(self._screen,best,height)
        #and save for later
        self._landingpad=landingpad_o
        
        #now the base this is located near the pad (within 4 pad sizes)
        #first, randomly select a pad
        baseimg=random.choice(self._baseimgs)
        basewidth=baseimg.get_width()
        #and the credible areas
        base_o=baseplayer.base(baseimg)
        basex,leftselector=terrain.findarea(basewidth,base_o.suitabilitychecker,best-basewidth*3,best+padwidth+basewidth*3)
        if not basex is None:
            #was n area, place
            #flatten area
            basey=terrain.createfoundation(basex,basewidth)
            #and place the base
            base_o.on_init(self._screen,basex,basey)       
            #and return
            return [landingpad_o],[base_o]
        else:
            #no spot to put base, so just no base
            return [landingpad_o],[]
            
    
    def exit(self):
        """
        Exit the game
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self._game.on_exit()
        
    def crash(self):
        """
        Crash the ship. Sessentially jst extends base to enable scoring
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #super
        super().crash()
        #calc score
        self.calculatescore(crashed=True)
        self._dlg=dialogue(
            not self.crashed,self.score,self._scoredict,self._highscores,self._screen,self._manager,self.exit,"skilltester"
            )
    
    
class dialogue(baseplayer.dialogue):
    """
    Class to show a dialogue box when crashed/landed
    """
    def __init__(self,landed,score,stats,highscores,screen,manager,cbmenu,mode):
        """
        Initialise
        
        INPUTS:
        landed: bool: did we land (true) or crash (false)
        score: Final score
        stats: stats dict. passed to highscores but also some properties are displayed
        highscores: High scores object
        screen: pygame screen object
        manager: GUI manager (pygame_gui)
        cbmenu: Callback for main menu button press
        mode: Game mode
        
        OUTPUTS:
        None
        """
        #save params
        self._landed=True #so save works
        self._score=score
        self._stats=stats
        self._highscores=highscores
        self._screen=screen
        self._manager=manager
        self._cbmenu=cbmenu
        self._mode=mode
        #work out the rank of our score
        rank,total=self._highscores.rank(self._mode,self._score)
        #create text strings
        if stats["numlands"]==1:
            text="Good Job! you landed {} time<br>and scored {:.0f} points!".format(stats["numlands"],score)
        else:
            text="Good Job! you landed {} times<br>and scored {:.0f} points!".format(stats["numlands"],score)
        if landed:
            title="Not a scratch on 'er!"
        else:
            #title of box
            title="Too bad about the scratches"
            #text in box. 
            text+="...With a minor hiccup at the end"
        #draw ourselves on the screen
        #create rect
        self._mbrect=pygame.Rect(
                0,0,
                400,250)
        #centre it on the screen
        self._mbrect.center=(self._screen.get_width()/2,self._screen.get_height()/2)
        #and the window
        self._mbwindow=pygame_gui.elements.UIWindow(self._mbrect,
            manager=self._manager,
            window_display_title=title
            )
        #OK button closes the dialogue
        self._okbutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(280,150,80,40),
            text="OK",
            manager=self._manager,
            container=self._mbwindow
            )
        #Menu button goes ot main menu
        self._menubutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(180,150,80,40),
            text="Menu",
            manager=self._manager,
            container=self._mbwindow
            )
        #now text
        self._boxtxt=pygame_gui.elements.ui_text_box.UITextBox(
            html_text=text,
            relative_rect=pygame.Rect(0,0,368,80),
            manager=self._manager,
            container=self._mbwindow
            )
        #name entry box
        #heading
        self._savelabel=pygame_gui.elements.ui_label.UILabel(
            relative_rect=pygame.Rect(0,80,200,30),
            text="Save your score!",
            manager=self._manager,
            container=self._mbwindow
            ) 
        #lable for text box
        self._namelabel=pygame_gui.elements.ui_label.UILabel(
            relative_rect=pygame.Rect(0,110,80,30),
            text="Name",
            manager=self._manager,
            container=self._mbwindow
            )  
        #text box
        self._nameentry=pygame_gui.elements.ui_text_entry_line.UITextEntryLine(
            relative_rect=pygame.Rect(80,110,150,30),
            manager=self._manager,
            container=self._mbwindow
            )
        #save score button
        self._savebutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(230,110,80,30),
            text="Save",
            manager=self._manager,
            container=self._mbwindow
            )    
        