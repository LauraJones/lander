"""
Dialogues.py. Defines some dioalogue boxes for use in games

Boxes:
message: basic message box (shows OK)
"""

class message(pygame.sprite.Sprite):
    """
    Basic dialogue box for showing a message and "OK"
    """
    def __init__(self,width,height,title,message):
        """
        Initialisation
        """
        