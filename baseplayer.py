"""
Base player object. 
"""

"""
Smoke
Smoke is emitted every few ticks as a small grey circle
Starts with random size and velocity/direction. 
Each tick it gets smaller until it disappears
velocity/direction gradualyl harmonises to upward (relatively slowly)
"""



import pygame
from pygame.locals import *
import os.path
import cmath
import math
from scipy.stats import linregress
import random
import time
import pygame_gui
import json
from pathlib import Path

class baseplayer(pygame.sprite.Sprite):
    """
    baseplayer class is the player in he game
    contains base methods that can be used by other game modes
    """
    def __init__(self,options,settings,highscores,game):
        """
        Initalisation. saves parameters and sets initial ship state
        
        INPUTS:
        options: options (things like which image sprite to use, gravity, etc)
        settings: settings
        highscores: highscores object
        game: game object
        """
        super().__init__()
        #save options 
        try:
            self._imgfile=options["imgfile"]
        except:
            self._imgfile="rocket.png"
        try:
            self._thrustimgfile=options["imgfilethrust"]
        except:
            self._thrustimgfile="rocket_thrusting.png"
        try:
            self._crashimgfile=options["imgfilecrashed"]
        except:
            self._crashimgfile="rocket_crashed.png"
        try:
            self._landedimgfile=options["imgfilelanded"]
        except:
            self._landedimgfile="rocket_landed.png"
        #save settings
        self._gravity=settings["gravity"]
        self._mass=settings["shipmass"]
        self._thrusterforce=settings["thrusterforce"]
        self._turnforce=settings["turnforce"]
        self._turnviscosity=settings["turnviscosity"]
        self._autostraighten=settings["autostraighten"]
        self._maxlandingvelocity=settings["maxlandingvelocity"]
        self.fuel=settings["fuel"]
        self.initfuel=settings["fuel"]
        self._mapwidth=settings["mapwidth"]
        self._terrainoptions=options["pads"]
        #and velocity, thrust, angle defaults
        self._vx=0
        self._vy=0
        self.absvelocity=0
        self.thrusting=False
        self.landed=False
        self.crashed=False
        self.turndir=0 #-1=anti clockwise. 0=none, 1=clockwise
        self.angle=0
        self._anglevelocity=0
        self.coords=[0,0]
        self._screenleft=0
        self.fuelpc=1 
        self._iter=0    #is calulation speed
        #scoring vars
        self._scored=False #score on first landing
        self._initlocation=[0,0] #start location
        self._starttime=None #when game started
        self._dlg=None
        self._game=game
        self.paused=False #not paused
        self._pausedtime=0 #time we were paused for
        self.message=None
        self.messagetimeout=None
        self._messagecolour=[64, 255, 64]
        self._descriptionmessage=options["description"]
        self._gametype=options["name"]
        self._smokesettings=options["smoke"]
        self._inwater=False
        #scoring
        self.score=0
        self._scoredict={}
        self._nextcalc=time.time()
        self._settings=settings
        self._options=options
        
    
    def on_init(self,screen,x,y,manager):
        """
        Set up graphics, read image files etc
        
        INPUTS:
        screen: The screen to draw on
        x: Starting x location
        y: Starting y location
        manager: GUI manager for window
        
        OUTPUTS: 
        None
        """
        #save screen
        self._screen=screen
        self._manager=manager
        #and coords
        self.coords=[x,y]
        #initial location. needs to be a copy (byval)
        self._initlocation=[self.coords[0],self.coords[1]]
        #and start time
        self._starttime=time.time()
        
        #load the image files
        #Non thrusting
        fpath=os.path.join("images",str(Path(self._imgfile)))
        #load file
        self._nonthrustimage=pygame.image.load(fpath).convert_alpha()
        
        #and thrusting
        fpath=os.path.join("images",str(Path(self._thrustimgfile)))
        #load file
        self._thrustimage=pygame.image.load(fpath).convert_alpha()
        
        #landed
        fpath=os.path.join("images",str(Path(self._landedimgfile)))
        #load file
        self._landedimage=pygame.image.load(fpath).convert_alpha()
        
        #and crashed
        fpath=os.path.join("images",str(Path(self._crashimgfile)))
        #load file
        self._crashimage=pygame.image.load(fpath).convert_alpha()
        
        #and image defaults to non thrusting
        self.image=self._nonthrustimage        
        #determine size
        self._imgsize=self.image.get_rect().size
        #make a rect
        self.rect=pygame.Rect(x,y,self._imgsize[0],self._imgsize[1])
        
        #and fonts
        self._bigfont=pygame.font.Font('tintin.ttf', 24)
        
        #List of smoke entries
        self._smokeobjects=[]

    def showwelcome(self):
        """
        Show the welcome message
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #pause
        self.paused=True
        self.pausestatus()
        #show message
        self._dlg=message(self._descriptionmessage,self._gametype,self.unpause,self._screen,self._manager)

    def unpause(self):
        """
        Unpause the game
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self.paused=False
        self.pausestatus()
        
    def on_event(self, event):
        """
        GUI event handler. all things that have events (keypresses and the like) are here
        
        INPUTS:
        event: the event to process
        
        OUTPUTS:
        None
        """
        #first, exit if the event type is quit
        if event.type == pygame.QUIT:
            self._running = False
        #buttons
        if event.type==pygame.USEREVENT:
            if event.user_type==pygame_gui.UI_BUTTON_PRESSED:
                #else: #uncomment when we have buttons
                if not self._dlg is None:
                    self._dlg.on_event(event)
        
    def update(self,screenleft):
        """
        Update method. blits ourselves onto the screen
        
        INPUTS:
        screenleft: The left side of the screen
        
        OUTPUTS: 
        None
        """
        #draw ourselves
        self._screen.blit(self.image, self.rect)
        #get rect
        self.rect=self.image.get_rect(
            center=Rect(self.coords[0],self.coords[1],1,1).center
            )
        #positon it
        self.rect.midbottom=[self.coords[0]-screenleft,self.coords[1]]
        # self.rect.x=self.coords[0]-screenleft
        # self.rect.y=self.coords[1]
        #save left of screen
        self._screenleft=screenleft
        
        #message
        if not self.message is None:
            #make text on surface
            msgtxt=self._bigfont.render(self.message,False,self._messagecolour)
            #and message rect
            messagerect=pygame.Rect(0,0,msgtxt.get_width(),msgtxt.get_height())
            messagerect.center=(self._screen.get_width()/2,self._screen.get_height()/2)            
            self._screen.blit(msgtxt,messagerect)
            #hide if needed
            if not self.messagetimeout is None:
                if self.messagetimeout<time.time():
                    self.message=None
                    
        #smoke
        #We draw between 0 and maxpertick smoke items per tick (if there is space)
        #and we are thrusting/chances turn out right
        ss=None
        #get the height of the ship
        height=self._thrustimage.get_height()/2
        if self.thrusting and not self.crashed:
            ss=self._smokesettings["flight"]
            angle=self.angle
            yoff=0
        elif self.crashed and not self._inwater:
            ss=self._smokesettings["crash"]
            angle=0
            yoff=-height
        if not ss is None:
            generate=random.randint(0,100)<ss["probability"]
            ticksmoke=random.randint(0,
                min([
                    ss["maxpertick"],
                    ss["maxnum"]-len(self._smokeobjects),
                    generate*1000
                    ])
                )
            #now make any requested
            for x in range(ticksmoke):
                #First, work out the x,y to generate from, based on the angle of the ship
                #we need to work out where the fire is coming from
                #now calculate the x/y offset of the fire
                xdiff=math.sin(angle)*height
                ydiff=height-math.cos(angle)-height+yoff
                #now location
                loc=[self.coords[0]+xdiff,self._screen.get_height()-(self.coords[1]+ydiff)]
                #now calculate the angle for the smoke to come out
                sangle=self.angle \
                    + math.radians((random.random()*(ss["maxangle"]-ss["minangle"]))) \
                    + math.radians(ss["minangle"])
                #now the velocity
                svel=random.random()*ss["initvel"]
                #Now create object
                sitm=smoke(self._smokesettings["particle"])
                sitm.on_init(
                    self._screen,
                    loc[0],
                    loc[1],
                    [svel*math.sin(sangle),svel*math.cos(sangle)]
                    )
                self._smokeobjects.append(sitm)

        #now drawing the items
        delitems=[]
        for i,o in enumerate(self._smokeobjects):
            o.update(screenleft)
            if o.width<1:
                delitems.append(i)
        #now delete missign objects
        #sort max to min
        delitems=sorted(delitems,reverse=True)
        for i in delitems:
            del self._smokeobjects[i]
        
        ##################################temp
        # if self.crashed:
            # for b in self._TEMP_boxes:
                # pygame.draw.rect(self._screen,(0,255,0),b)
    
    def event(self,event):
        """
        Process events. Events are:
        Space: Fire boosters
        Left/right arrows: Rotate
        
        INPUTS:
        event: The event to process
        
        OUTPUTS: 
        None
        """
        #keydown events are when a key is pressed
        if event.type == pygame.KEYDOWN:
            if event.key==K_SPACE:
                #thrust on
                self.thrusting=True
            if event.key==K_RIGHT:
                #Right arrow, clockwise
                self.turndir=-1
            if event.key==K_LEFT:
                #Left arrow, anticlockwise
                self.turndir=1
            if event.key==K_PAUSE:
                #pause key
                self.paused=not self.paused
                self.pausestatus()
                
        if event.type == pygame.KEYUP:
            if event.key==K_SPACE:
                #thrust off
                self.thrusting=False
            if event.key==K_RIGHT or event.key==K_LEFT:
                #No angle
                self.turndir=0
    
    def pausestatus(self):
        """
        Do the bits otmake pause work (other than flag)
         - show/hide message
         - record paused time
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #save time
        if self.paused:
            self._pausestart=time.time()
        else:
            thispause=time.time()-self._pausestart
            self._pausedtime+=thispause
        #message
        if self.paused:
            self.showmessage("Paused")
        else:
            self.hidemessage()
    
    def loop(self):
        """
        Loop. does physics etc
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #exit if paused
        if self.paused:
            return
        #set velocity
        self.absvelocity=(self._vx**2+self._vy**2)**0.5
        #return if crashed
        if self.crashed:            
            return
        #do fuel, and alter flags if we are out
        if self.thrusting:
            self.fuel-=1
            self.fuelpc=self.fuel/self.initfuel 
        if self.fuel<=0:
            #out of fuel, so never tyhrusting
            self.thrusting=False
            self.turndir=0
            self.fuel=0
        #now we only do this on every second iteration
        if self._iter==0:
            #Turn thrust. really about moments but this will do
            turnacc=self.turndir*self._turnforce/self._mass
            #and viscosity is related to speed
            turnacc-=self._anglevelocity*self._turnviscosity
            #and angle velocity
            self._anglevelocity+=turnacc*1/60
            #and angle
            self.angle+=self._anglevelocity
            #and if we have autoreturn and arent turning, return to zero slowly
            if self.turndir==0 and self._autostraighten:
                if self.angle>0:
                    self.angle-=0.1
                    self.angle=max([self.angle,0])
                else:
                    self.angle+=0.1
                    self.angle=min([self.angle,0])
            #do force balances. real = up/down, imag=left/right (+/-)
            vector=cmath.rect(1,self.angle)
            mainthruster=vector*complex(
                -self.thrusting*self._thrusterforce,
                0
                )
            #gravity only applies if not landed
            if not self.landed:
                gravity=complex(
                    self._gravity*self._mass,
                    0
                    )
            else:
                #else no gravity
                gravity=complex(0,0)        
            
            #and now net force
            netforce=gravity+mainthruster
            #acceleration (f=ma)
            acc=netforce/self._mass
            #delta velocity
            dv=acc*1/60 #(seconds)
            #and add to velocity
            self._vx+=dv.imag
            self._vy+=dv.real
           
        
        #move
        self.coords[0]=self.coords[0]+self._vx
        self.coords[1]=self.coords[1]+self._vy
        #Screen edge behaviour
        if self.rect.x>=(self._screen.get_width()-self._imgsize[0]):
            #off right edge. want ony negative velocities
            self._vx=min([0,self._vx])
            self.rect.x=self._screen.get_width()-self._imgsize[0]
        elif self.rect.x<=0:
            #off left edge, wan tonly positive
            self._vx=max([0,self._vx])
            self.rect.x=0
        if self.rect.y>=(self._screen.get_height()-self._imgsize[1]):
            #off bottom, shoudl not happe, but only negative velocity wnated
            self._vy=min([0,self._vy])
            self.rect.y=self._screen.get_height()-self._imgsize[1]
        elif self.rect.y<=0:
            #off top. only positive veolocities
            self._vy=max([0,self._vy])
            self.rect.y=0
        #now update rect, image
        #rotate the image
        #angle from radians to degrees
        angle=math.degrees(self.angle)
        if self.thrusting:
            self.image=pygame.transform.rotate(self._thrustimage,angle)
        elif self.landed:
            self.image=self._landedimage
        else:
            self.image=pygame.transform.rotate(self._nonthrustimage,angle)
        
        #and ieration
        self._iter+=1
        self._iter=self._iter%2
        
        #now calculate the score if needed
        if time.time()>self._nextcalc:
            self.score,self._scoredict=self.calculatescore()
            self._nextcalc=time.time()+2
        
    
    def terrain(self):
        """
        Assess terrain to see if we crashed
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.crashed or self.landed:
            return
        #Check if we crashed
        terrainflag,xterrain,xscreen=self._terrain.terraincheck(self.rect)
        if terrainflag==1:
            #underwater disable thrust
            self.thrusting=False
            self._inwater=True
        elif terrainflag==2:
            #crashed, Get angle
            #first, work out the leftmost point of the ship
            #first, work out where we hit along hte bottom of the ship
            hitpoint=(xscreen-self.rect.x)/self.rect.width
            #now get terrain limits under ourselves
            minx=xterrain-hitpoint*self.rect.width
            maxx=xterrain+(1-hitpoint)*self.rect.width
            #now work out angle
            self.angle=self._terrain.getangle(minx,maxx)
            #and crash
            self.crash()
        else:
            self._inwater=False
            
        # self.landed=False
        # for terrain in terrains:
            # if self.rect.colliderect(terrain.rect):
                # #crashed
                # self.crash(terrain,terrains)                    
                
     
    def crash(self):
        """
        Crash the ship. Ship is rotated to match terrain if it is in terrain
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        print("crash")
        #we cant crash twice
        if self.crashed:
            return
        #set crashed flag
        self.crashed=True
        #and set velocity to zero
        self._vx=0
        self._vy=0
        #and place the image
        self.image=pygame.transform.rotate(self._crashimage,self.angle)
        #get rect
        self.rect=self.image.get_rect(
            center=Rect(self.coords[0],self.coords[1],1,1).center
            )        

    def exit(self):
        """
        Exit the game
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self._game.on_exit()
        
    def showmessage(self,message,timeout=None):
        """
        Show a message on the screen, with an optional timeout
        
        INPUTS:
        message: Message to show
        timeout: Timeout in seconds
        
        OUTPUTS:
        None
        """
        #save message and timeout
        self.message=message
        if not timeout is None:
            self.messagetimeout=time.time()+timeout
        else:
            self.messagetimeout=None
        
    def hidemessage(self):
        """
        Hide message
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self.message=None
        
    def calculatescore(self):
        """
        Dummy calculate score method
        
        INPUTS:
        None
        
        OUTPUTS:
        score, scoring parameters
        """
        return 0,{}
        
        
class landingpad(pygame.sprite.Sprite):
    """
    Landing pad. This is a green bar on top of the terrain 
    """
    def __init__(self,height,width,colour):
        """
        Initialisation
        
        INPUTS:
        height: Height of the bar
        width: width of landign pad
        colour: colour of the landing pad
        """
        super().__init__()
        #now save settings
        self.width=width
        self.height=height
        self.colour=colour
        self.destination=True #always next pad ot land on
        
    def on_init(self,screen,x,y):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        x: our x location
        y: our y location
        
        OUTPUTS: 
        None
        """
        self.terrainx=x
        self.terrainy=y
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #and our rect
        self.rect=pygame.Rect(self.terrainx,self.screenheight-self.terrainy,self.width,self.height)
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        self.rect.x=self.terrainx-screenleft
        if (self.terrainx+self.width)>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            pygame.draw.rect(self._screen,self.colour,self.rect)
            
    def suitabilitychecker(self,area):
        """
        Check the suitability of an area for base placement
        
        INPUTS:
        area: a list of heights ot check
        
        OUTPUTS:
        a number that gets higher as the site gets better
        """
        return min(area)-max(area)

class base(pygame.sprite.Sprite):
    """
    Base. This is a base image to display on the screen
    """
    def __init__(self,image):
        """
        Initialisation
        
        INPUTS:
        image: image surface to display
        """
        super().__init__()
        self.image=image
        
        
    def on_init(self,screen,x,y):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        x: our x location
        y: our y location (height form bottom)
        
        OUTPUTS: 
        None
        """
        #now save settings
        self.terrainx=x
        self.terrainy=y
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #and our rect
        self.rect=self.image.get_rect()
        self.rect.bottomleft=(self.terrainx,self.screenheight-self.terrainy)
        #and save width
        self.width=self.rect.width
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        self.rect.x=self.terrainx-screenleft
        if (self.terrainx+self.width)>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            self._screen.blit(self.image, self.rect)
            
    def suitabilitychecker(self,area):
        """
        Check the suitability of an area for base placement
        
        INPUTS:
        area: a list of heights ot check
        
        OUTPUTS:
        a number that gets higher as the site gets better
        """
        return min(area)-max(area)
        
class smoke(pygame.sprite.Sprite):
    """
    Smoke object is a single bit of smoke 
    """
    def __init__(self,settings):
        """
        Initialisation
        
        INPUTS:
        settings: Settings
        """
        super().__init__()
        self._idealy=settings["idealy"]
        self._idealx=settings["idealx"]
        self._xrandomness=settings["xrandomness"]
        self._yrandomness=settings["yrandomness"]
        self._maxlife=settings["maxlife"]
        self._colour=settings["colour"]
        
    def on_init(self,screen,x,y,velocity):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        x: our x location
        y: our y location (height from bottom)
        velocity: 2 element tuple of velocity (x,y)
        
        OUTPUTS: 
        None
        """
        #now save settings
        self.terrainx=x
        self.terrainy=y
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        self._velocity=velocity
        #life
        self._life=random.randint(1,self._maxlife)
        #now make ourselves. we are 1px wide circle
        self.width=1
        #assume screenleft is 0 for now, is wrong but we dont know any better
        self.rect=pygame.draw.circle(
            self._screen,
            self._colour,
            (self.terrainx,self.screenheight-self.terrainy),
            self.width
            )
        
    def update(self,screenleft):
        """
        Calculate motion and draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        #first, calculate motion
        #we use existing velocity to move
        self.terrainx+=self._velocity[0]
        self.terrainy+=self._velocity[1]
        #now we alter to make it move more like smoke
        #We eventually want to end up with vertical velocity of 2 (changeable...?)
        #and a random horizontal velocity between 1 and -1
        if self._velocity[0]>self._idealx:
            self._velocity[0]-=random.random()*self._xrandomness
        if self._velocity[0]<self._idealx:
            self._velocity[0]+=random.random()*self._xrandomness
        if self._velocity[1]>self._idealy:
            self._velocity[1]-=random.random()*self._yrandomness
        if self._velocity[1]<self._idealy:
            self._velocity[1]+=random.random()*self._yrandomness

        #now dissappear (set width to zero) randomly
        if self._life<1:
            self.width=0
        self._life-=1
        #now draw ourselves
        if (self.terrainx+self.width)>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            self.rect=pygame.draw.circle(
                self._screen,
                (128,128,128),
                (self.terrainx-screenleft,self.screenheight-self.terrainy),
                self.width
                )    
        
class dialogue:
    """
    Class to show a dialogue box when crashed/landed
    """
    def __init__(self,landed,score,stats,highscores,screen,manager,cbmenu,mode):
        """
        Initialise
        
        INPUTS:
        landed: bool: did we land (true) or crash (false)
        score: Final score
        stats: stats dict. passed to highscores but also some properties are displayed
        highscores: High scores object
        screen: pygame screen object
        manager: GUI manager (pygame_gui)
        cbmenu: Callback for main menu button press
        mode: Game mode
        
        OUTPUTS:
        None
        """
        #save params
        self._landed=landed
        self._score=score
        self._stats=stats
        self._highscores=highscores
        self._screen=screen
        self._manager=manager
        self._cbmenu=cbmenu
        self._mode=mode
        #work out the rank of our score
        rank,total=self._highscores.rank(self._mode,self._score)
        #create text strings
        if self._landed:
            if rank<1:
                title="Good Job!"
                text="You scored {:.0f} points!".format(self._score)
            elif rank==1:
                title="New record!"
                text="<font color='#FFD700' size=5>Gold medal captain!</font><br>You scored {:.0f} points, more than anyone else! <br>Be sure to save this one!".format(self._score)
            elif rank==2:
                title="Stellar piloting!"
                text="<font color='#C0C0C0' size=5>Super Second!</font><br>You scored {:.0f} points: Second place is not a bad effort!".format(self._score)
            elif rank==3:
                title="Stellar piloting!"
                text="<font color='#CD7F32' size=5>Terrific Third!</font><br>You scored {:.0f} points: Definitely worthy of a medal!".format(self._score)
            elif rank<(0.3*total):
                title="Stellar piloting!"
                text="<font color='#00FF00' size=5>Super Captain! Only {:.0%} of scores are higher than yours!</font><br>You scored {:.0f} points, ranking {:d}/{:d}!".format(
                    (rank-1)/total,self._score,rank,total)
            elif rank<(0.5*total):
                title="Good Job!"
                text="<font color='#00FF00' size=5>Expert piloting! You scored better than {:.0%} of other scores </font><br>At {:.0f} points, your rank was {:d}/{:d}!".format(
                    1-(rank-1)/total,self._score,rank,total)
            else: 
                title="Made it!"
                text="<font color='#FF0000' size=5>Whew, made it!</font><br>At {:.0f} points, your rank was {:d}/{:d}. Better luck next time!".format(
                    self._score,rank,total)           
        else:
            #title of box
            title="Oh No!"
            #text in box. 
            text="<font color='#FF0000'>You crashed!</font>"
        #draw ourselves on the screen
        #create rect
        self._mbrect=pygame.Rect(
                0,0,
                400,250)
        #centre it on the screen
        self._mbrect.center=(self._screen.get_width()/2,self._screen.get_height()/2)
        #and the window
        self._mbwindow=pygame_gui.elements.UIWindow(self._mbrect,
            manager=self._manager,
            window_display_title=title
            )
        #OK button closes the dialogue
        self._okbutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(280,150,80,40),
            text="OK",
            manager=self._manager,
            container=self._mbwindow
            )
        #Menu button goes ot main menu
        self._menubutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(180,150,80,40),
            text="Menu",
            manager=self._manager,
            container=self._mbwindow
            )
        #now text
        self._boxtxt=pygame_gui.elements.ui_text_box.UITextBox(
            html_text=text,
            relative_rect=pygame.Rect(0,0,368,80),
            manager=self._manager,
            container=self._mbwindow
            )
        if self._landed:
            #name entry box
            #heading
            self._savelabel=pygame_gui.elements.ui_label.UILabel(
                relative_rect=pygame.Rect(0,80,200,30),
                text="Save your score!",
                manager=self._manager,
                container=self._mbwindow
                ) 
            #label for text box
            self._namelabel=pygame_gui.elements.ui_label.UILabel(
                relative_rect=pygame.Rect(0,110,80,30),
                text="Name",
                manager=self._manager,
                container=self._mbwindow
                )  
            #text box
            self._nameentry=pygame_gui.elements.ui_text_entry_line.UITextEntryLine(
                relative_rect=pygame.Rect(80,110,150,30),
                manager=self._manager,
                container=self._mbwindow
                )
            #save score button
            self._savebutton=pygame_gui.elements.ui_button.UIButton(
                relative_rect=pygame.Rect(230,110,80,30),
                text="Save",
                manager=self._manager,
                container=self._mbwindow
                )    
        
    def on_event(self, event):
        """
        Event handler. all things that have events (keypresses and the like) are here
        
        INPUTS:
        event: the event to process
        
        OUTPUTS:
        None
        """
        #first, exit if the event type is quit
        if event.type == pygame.QUIT:
            self._running = False
        #buttons
        if event.type==pygame.USEREVENT:
            if event.user_type==pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element==self._okbutton:
                    #hide ourselves
                    self._mbwindow.kill()
                if event.ui_element==self._menubutton:
                    #hide ourselves
                    self._mbwindow.kill()
                    #run callback
                    self._cbmenu()
                if self._landed:
                    if event.ui_element==self._savebutton:
                        #Get name
                        sn=self._nameentry.get_text()
                        #get list of previous scores so see where we rank
                        rank,total=self._highscores.rank(self._mode,self._score,sn)
                        #save score
                        self._highscores.save(self._mode,self._score,sn,self._stats)
                        #now get playerstats
                        #show a message
                        rct=pygame.Rect(
                                0,0,
                                280,220)
                        #centre it on the screen
                        if total>0:
                            if rank==1:
                                st="New record"
                                col="#FFD700"
                            elif rank==2:
                                st="Second place"
                                col="#C0C0C0"
                            elif rank==3:
                                st="Third place"
                                col="#CD7F32"
                            elif rank<(0.8*total):
                                st="Good Job"
                                col="#00FF00"                       
                            else: 
                                st="Better luck next time"
                                col="#FF0000"
                            html="<font color='{}' size=5>{}, {}!</font><br> Your score of {:.0f} ranks {:d} out of all your {:d} scores".format(
                                col,st,sn,self._score,rank,total+1)
                        else:
                            html="<font color='#00FF00' size=5>Congratulations on your first save, {}!</font>".format(sn)
                        rct.center=(self._screen.get_width()/2,self._screen.get_height()/2)
                        pygame_gui.windows.ui_message_window.UIMessageWindow(
                            rect=rct,
                            html_message=html,
                            manager=self._manager,
                            window_title="Score saved!"
                            )
                        #and remove the save and name boxes
                        self._savebutton.disable()
                        self._savebutton.hide()
                        self._nameentry.disable()
                        self._nameentry.hide()
                        self._savelabel.hide()
                        self._namelabel.hide()
                    
                          
class message:
    """
    Class to show a message (OK only option)
    """
    def __init__(self,message,title,callback,screen,manager):
        """
        Initialise
        
        INPUTS:
        message: the message to show (html format)
        title: title of window
        callback: callback to call when OK is pressed
        screen: the pygame screen object
        manager: GUI manager (pygame_gui)
        
        OUTPUTS:
        None
        """
        #save params
        self._message=message
        self._title=title
        self._screen=screen
        self._manager=manager
        self._callback=callback
        
        #draw ourselves on the screen
        #create rect
        self._mbrect=pygame.Rect(
                0,0,
                600,400)
        #centre it on the screen
        self._mbrect.center=(self._screen.get_width()/2,self._screen.get_height()/2)
        #and the window
        self._mbwindow=pygame_gui.elements.UIWindow(self._mbrect,
            manager=self._manager,
            window_display_title=title
            )
        #OK button closes the dialogue
        self._okbutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(440,280,80,40),
            text="OK",
            manager=self._manager,
            container=self._mbwindow
            )       
        #now text
        self._text=pygame_gui.elements.ui_text_box.UITextBox(relative_rect=pygame.Rect((0, 0), (440, 400)),
            html_text=self._message,
            manager=self._manager,
            container=self._mbwindow
            )  
        
    def on_event(self, event):
        """
        Event handler. all things that have events (keypresses and the like) are here
        
        INPUTS:
        event: the event to process
        
        OUTPUTS:
        None
        """
        #first, exit if the event type is quit
        if event.type == pygame.QUIT:
            self._running = False
        #buttons
        if event.type==pygame.USEREVENT:
            if event.user_type==pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element==self._okbutton:
                    #hide ourselves
                    self._mbwindow.kill()
                    #call callback
                    self._callback()
