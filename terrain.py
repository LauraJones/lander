"""
terrain.py 
Builds terrian, landing pad, and scenery. 

Terrain:
Terrain is randomly generated and undulates between max and min height. Random slopes 
generate an undulating, random terrain.
Each terrain element is a 1 pixxel wide rectangle and is defined by the terrainelement 
object

Landing Pad
Landing pad is placed on the flattest piece of terrain. the terrian is smoothed and the
pad is placed on top of it.
landing pad is defined by the landing pad element
"""

import pygame
from pygame.locals import *
import os.path
import cmath
import math
import random
from scipy.stats import linregress
from pathlib import Path

terrain_avoid=-999999 #a big negative number signals to avoid placing elements here

class terrain:
    """
    Rocket class is the player
    """
    def __init__(self,options,settings):
        """
        Initalisation. saves parameters and builds terrain
        
        INPUTS:
        options: options (terrain settings)
        settings: settings around game type/difficulty (things like gravity)
        
        OUTPUTS:
        None
        """
        #save parameters
        self._settings=settings
        self._options=options
        
    def on_init(self,screen,player):
        """
        Set up graphics, read image files etc
        
        INPUTS:
        screen: the screen to draw on
        player: player object
        
        OUTPUTS:
        trrain gorup, pad group: groups of elemetns that are drawn on the sceeen
        """
        self._screen=screen
        #first build the heightmap
        self._buildterrain()
        #now add oceans etc
        self._oceans()
        self._snow()
        #now create the landing pad, etc
        self._pads,self._scenery=player.addlandingpads(self)
        #Now villages come last
        self._villages()
        return self._creategroup()
        
        
    def _villages(self):
        """
        Villages: These are clusters of sprites that represent a village. 
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #first, empty village sprites list
        self._villagelist=pygame.sprite.RenderUpdates()
        villagecentres=[] #list of centre x coords of existing villages
        #return if no villages
        if not self._options["villages"]:
            return
        #get settings
        os=self._options["villagesettings"]
        #now we need to find a good spot to put the village.
        #They are (for now) randomly placed.
        #first work out how many to place
        nvillages=random.randint(os["min"],os["max"])
        if not nvillages:
            #return if none
            return
        #and buffer - how far to space them
        buffer=0.2*self._settings["mapwidth"]/nvillages
        #now go through and generate
        for v in range(nvillages):
            #generate a village
            #first, select the type
            vtype=random.choice(list(os["types"].keys()))
            #work out where to put it
            loc=random.randint(0,self._settings["mapwidth"])
            if (len(villagecentres)>0):
                while min([abs(loc-x) for x in villagecentres])<os["mindist"]:
                    loc=random.randint(0,self._settings["mapwidth"])
            villagecentres.append(loc)
            #make it
            bldgs=self._makevillage(os["types"][vtype],loc)
            #add to list
            for b in bldgs:
                self._villagelist.add(b)
    
    def _makevillage(self,vset,loc):
        """
        Put a village on the map. 
        Village is centred on x location loc. is built uding settings in vset
        
        INPUTS:
        vset: village settings: buildings, generation profiles, etc
        loc: X location of village
        
        OUTPUTS:
        list of bulidings
        """
        bldgs=[]
        #first, read in all the images
        selector=[] #used to pick a building
        frequency={} #dict that shows the number of each building type
        #and work out average building width
        tw=0
        for name,bldg in vset["buildings"].items():
            #path
            fpath=os.path.join("images",str(Path(bldg["file"])))
            #load file
            vset["buildings"][name]["img"]=pygame.image.load(fpath).convert_alpha()
            #now selector
            sl=[name for x in range(bldg["probability"])]
            selector+=sl
            #frequency
            vset["buildings"][name]["frequency"]=0
            #and width
            tw+=vset["buildings"][name]["img"].get_width()
        #average width
        aw=tw/len(vset["buildings"].keys())
        #now place.
        #work out how many buildings to have
        nbldg=random.randint(vset["minsize"],vset["maxsize"])
        #and village width
        vw=nbldg*aw*vset["sparsity"]
        #translate to x lims
        minx=int(max([0,loc-vw/2]))
        maxx=int(min([self._settings["mapwidth"],vw-(loc-minx)+loc]))
        if "maxslope" in vset.keys():
           mxslope=vset["maxslope"]
        else:
            mxslope=None
        #now draw the items
        def assess(area,mxslope=mxslope):
            #random placement. optionally avoid areas which are too slopey
            rslope=random.random()
            if mxslope is None:
                return rslope
            else:
                if abs(max(area)-min(area))/len(area)>mxslope:
                    return terrain_avoid
                else:
                    return rslope
            
        for b in range(nbldg):
            #work out what building to place
            bldg=random.choice(selector)
            #now go until its valid
            while vset["buildings"][bldg]["frequency"]>=vset["buildings"][bldg]["max"]:
                bldg=random.choice(selector)
            bldg_s=vset["buildings"][bldg]["img"]
            #now resize it if desired
            if "resize" in vset["buildings"][bldg].keys():
                #get range
                rng=vset["buildings"][bldg]["resize"]
                scale=random.uniform(rng[0],rng[1])
                newwidth=int(scale*bldg_s.get_width())
                newheight=int(scale*bldg_s.get_height())
                #scale
                bldg_s=pygame.transform.scale(bldg_s,(newwidth,newheight))
            #now place the building
            bw=bldg_s.get_width()
            xloc,factor=self.findarea(bw,assess,minx,maxx)
            if xloc is None:
                #no area found to terminate generation
                break
            #put it there
            bldg_o=house(bldg_s)
            yloc=self.createfoundation(xloc,bw,11,vset["flatten"])
            bldg_o.on_init(self._screen,xloc,yloc)
            bldgs.append(bldg_o)
            #and increment incidence
            vset["buildings"][bldg]["frequency"]+=1
        return bldgs
        
        
    def _oceans(self):
        """
        Add oceans. Oceans fill valleys up to a random height.
        Not all valleys contain oceans, just where randomly generated.
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #empty lake list
        self._lakegrp=pygame.sprite.RenderUpdates()
        #return if no ocean
        if not self._options["oceans"]:
            self._lakecrashbehaviour="crash"
            return
        #get settings
        os=self._options["oceansettings"]
        self._lakecrashbehaviour=os["crashbehaviour"]
        #first, calc the height. First, get a sorted list of heights 
        sh=sorted(self._heightmap) #ascending order
        #get an index
        idx=random.choice(
            range(
                int(os["mincover"]*len(sh)),
                int(os["maxcover"]*len(sh))
                ))
        #and the value below which we might put an ocean
        mv=sh[idx]
        #now go through the heightmap and put oceans in
        clake=False #true if currently a lake
        lh=self._heightmap[0] #last height
        for x,ch in enumerate(self._heightmap):
            #check ofr lake start
            if ch<mv and lh>mv:
                #first entry below, so see if we make a lake
                if random.random()<os["chance"]:
                    clake=True
            #now for lake end
            if ch>mv:
                clake=False
            #now make a lake
            if clake:
                #make element
                le=lakeelement(x,ch,mv-ch,os["colour"])
                le.on_init(self._screen)
                self._lakegrp.add(le)
                #and set flag
                self._objects[x]=10
            lh=ch
        
    def _snow(self):
        """
        Generate snow. Snow can be in valleys (hidden from the sun) or on peaks
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #empty snow list
        self._snowgrp=pygame.sprite.RenderUpdates()
        #return if no snow
        if not self._options["snow"]:
            return
        #set settings
        os=self._options["snowsettings"]
        #first, calc the height. First, get a sorted list of heights 
        sh=sorted(self._heightmap) #ascending order
        #get an index
        idx=random.choice(
            range(
                int(os["minheight"]*len(sh)),
                int(os["maxheight"]*len(sh))
                ))
        #critical vlaue
        mv=sh[idx]
        #and actual value is this plus a randomness
        av=mv+random.randint(-os["hrandomness"],os["hrandomness"])
        av=min(max(self._heightmap),av)
        av=max(min(self._heightmap),av)
        csnow=False #and currently snow
        hsnow=False #and we hide it sometimes 
        lh=self._heightmap[0] #last height
        depth=0    #last depth
        cslope=0    #last slope
        #now we need to do the generation. We can generate in valleys or on peaks
        for x,ch in enumerate(self._heightmap):            
            if csnow:
                #is snow, so we need to check if we are ending it
                if os["generate"]=="valleys":
                    #look for upward slopes
                    favourable=ch>lh and ch>av
                else:
                    #downward slopes (near top of hills)
                    favourable=ch<lh and ch<av
                #now work out if we should end
                csnow=not(favourable)
                #now generate a new height if we need to
                if not csnow:
                    av=mv+random.randint(-os["hrandomness"],os["hrandomness"])
            else:
                #No snow, check for start
                #snow can start at any favourable slope that is beyond a random offset of the height
                if os["generate"]=="valleys":
                    #look for downward slopes
                    favourable=ch<lh and ch<av
                else:
                    #upward slopes (near top of hills)
                    favourable=ch>lh and ch>av
                #now work out if we should start
                csnow=favourable
                if csnow:
                    #max height is limited to increase in a slope to maximum. slope is set in config
                    p_av=av
                    #generate new av
                    av=mv+random.randint(-os["hrandomness"],os["hrandomness"])
                    #and work out wheter to supress it
                    hsnow=random.random()>os["chance"]
            #now make a snow cap/valley
            if csnow and not hsnow:
                #calculate height. We make it shallower near the edge and randomly vary within
                cslope=max([
                        min([
                            random.randint(-os["roughness"]*10,os["roughness"]*10)/10,os["maxslope"]
                            ]),-os["maxslope"]
                            ])   
                depth+=cslope
                if os["generate"]=="valleys":
                    edgelimit=(p_av-ch)*os["edgeslope"]
                else:
                    edgelimit=(ch-p_av)*os["edgeslope"]
                depth=max([min([os["depth"],abs(ch-mv),depth,edgelimit]),0])
                #and set av to the downward slope if we get to max depth
                if edgelimit>=os["depth"]:
                    p_av=av
                #and ensure slope is OK if ad edges
                if depth<=1:
                    cslope=max([
                        min([
                            random.randint(0,os["roughness"]*10)/10,os["maxslope"]
                            ]),-os["maxslope"]
                            ]) 
                if depth>=os["depth"]:
                    cslope=max([
                        min([
                            random.randint(-os["roughness"]*10,0)/10,os["maxslope"]
                            ]),-os["maxslope"]
                            ]) 
                #now draw it. Same as lake element
                se=lakeelement(x,ch,depth,os["colour"])
                se.on_init(self._screen)
                self._snowgrp.add(se)
                #and set flag
                self._objects[x]=1
            lh=ch
        
    
    def _buildterrain(self):
        """
        Build terrain. THis generates the heightmap rather than the terrain 
        itself. the terrain is drawn later 
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #get start, max, min, roughness
        self._heightmap=[0 for x in range(self._settings["mapwidth"])] #one each column
        #Objects list. Will be <10 for items that can have others placed over the top, >10 for others. 0 is bare terrain 
        #0: Bare
        #1: Snow
        #10: Lake
        #11: village
        #20: item placed by something else
        self._objects=[0 for x in range(self._settings["mapwidth"])]    
        cheight=self._options["startheight"]
        maxheight=self._options["maxheight"]
        minheight=self._options["minheight"]
        roughness=self._options["roughness"] #roughness is how quickly slope changes. 
        maxslope=self._options["maxslope"] 
        #and we go slopes in 0.1 detents for more granularity
        cslope=random.randint(-roughness*10,roughness*10)/10
        for col in range(self._settings["mapwidth"]):
            #calc new height
            cheight+=cslope
            if cheight>maxheight:
                cheight=maxheight
                cslope=random.randint(-roughness*10,0)/10
            if cheight<=minheight:
                cheight=minheight
                cslope=random.randint(0,roughness*10)/10   
            #append to map
            self._heightmap[col]=cheight
            self._objects[col]=0
            #and alter slope
            cslope+=random.randint(-roughness*10,roughness*10)/10
            cslope=max([min([cslope,maxslope]),-maxslope])
        
  
    def _creategroup(self):
        """
        Create the return groups. There are two:
         - one for terrain
         - one for special items (landing pads etc)
        """
        #create terrain group
        self._terraingrp=pygame.sprite.RenderUpdates()
        for col,cheight in enumerate(self._heightmap):
            #and create elemetns
            ter=terrainelement(col,cheight,self._options["colour"])
            ter.on_init(self._screen)
            self._terraingrp.add(ter)
        #add pad goes in landing pad group
        self._padgrp=pygame.sprite.RenderUpdates()
        for p in self._pads:
            self._padgrp.add(p)
        #and the scenery
        self._scenerygrp=pygame.sprite.RenderUpdates()
        for s in self._scenery:
            self._scenerygrp.add(s)
        return self._terraingrp, self._padgrp, self._scenerygrp
        
    def getheightmap(self):
        """
        Return a height map 
        
        INPUTS:
        None
        
        OUTPUTS:
        hightmap (list of heights)
        """
        return self._heightmap
    
    def findarea(self,size,assessor,minx=None,maxx=None,avoidlakes=True):
        """
        Find an area to put an item. 
        Calls the assessor for each range of x values and returns the x index that
        gave the "best" (highest) value. Lakes are skipped if requied (default)
        
        INPUTS:
        minx:   Min x index to look from
        maxx:   Max x index to look to
        size:   Block size
        assessor: assessor function. Is passed list of heights to assess
        
        OUTPUTS:
        best x address to place element, and the best found assessor value
        bext x address is None if no place found
        """
        if minx is None:
            minx=0
        if maxx is None:
            maxx=len(self._heightmap)-size
        #first, exit if area is too small
        if (maxx-minx)<size:
            return None
        #and limit maxx,minx to valid range
        maxx=min([len(self._heightmap)-size,maxx])
        minx=max([0,minx])
        #now look for spots. first the first entry
        if max(self._objects[minx:minx+size])<10:
            best=assessor(self._heightmap[minx:minx+size])
        else:
            best=terrain_avoid
        bestx=minx
        for x in range(minx,maxx):
            #only do if none of the range is invalid
            if max(self._objects[x:x+size])<10:           
                cv=assessor(self._heightmap[x:x+size])
            else:
                #ensure never new best
                cv=terrain_avoid
            if cv>best:
                bestx=x
                best=cv
        #make cv None if is a big negative number
        if best<=terrain_avoid:
            print("None")
            best=None
            bestx=None
        #return best
        return bestx,best
    
    def createfoundation(self,x,width,flag=20,flatten=True):
        """
        Create a foundation (flat area) for an object. 
        It sets the height of all land in the area to the average
        also removes any snow
        
        INPUTS:
        x: the left-side x coordinate
        width: the width to flatten
        flag: the flag ot set terrain to. defaults to 20 (externally applied item)
        flatten: True toflatten the area, false ot leave as-is
        
        OUTPUTS:
        the height to draw the object
        """
        #get avg height (where we put it) and flatten if desired
        lh=sum(self._heightmap[x:x+width])/width
        if flatten:
            self._heightmap[x:x+width]=[lh for x in range(width)]
        #now remove snow
        toremove=[]
        for snow in self._snowgrp:
            #add to remove list if needed
            if snow.terrainx>=x and snow.terrainx<=(x+width):
                toremove.append(snow)
        #and remove
        for snow in toremove:
            self._snowgrp.remove(snow)
        #now return land height
        #now set flags for an item there
        self._objects[x:x+width]=[flag for x in range(width)]
        return lh
        
    def update_background(self,scrleft):
        """
        Update background elements (scenery)
        
        INPUTS:
        scrleft: Left side of screen
        
        OUPUTS:
        None
        """
        #draw terrain
        self._terraingrp.update(scrleft)
        #lakes
        self._lakegrp.update(scrleft)
        #snow
        self._snowgrp.update(scrleft)
        #and scenery
        self._scenerygrp.update(scrleft)
        #and villages
        self._villagelist.update(scrleft)
        
    def terraincheck(self,rect):
        """
        Check for interaction with terrain based on supplied rect.
        returns flag with several options:
        0: no interaction
        1: Disable thrust
        2: Crash
        
        INPUTS:
        rect: the rectangle to check
        
        OUTPUTS:
        flag, terrain x of crash
        """
        #first, land
        for terrain in self._terraingrp:
            if rect.colliderect(terrain.rect):
                #crashed
                return 2,terrain.terrainx,terrain.rect.x
        #now lakes
        for lake in self._lakegrp:
            if rect.colliderect(lake.rect):
                #crashed. check what to do
                if self._lakecrashbehaviour=="extinguish":
                    return 1,lake.terrainx,lake.rect.x
                else:
                    #default to crash
                    return 2, lake.terrainx,lake.rect.x
        #nothing, so return 0
        return 0,0,0
    
    def getangle(self,minx,maxx):
        """
        Get the local angle of the terrain 
        
        INPUTS:
        minx: the x to start the check from
        maxx: the x ot end at
        
        OUTPUTS:
        angle
        """
        #and limit maxx,minx to valid range
        maxx=int(min([len(self._heightmap),maxx]))
        minx=int(max([0,minx]))
        #now get the terrain under the ship. 
        #there are two options: we could be on a solid lake or the ground
        terrains=[None for x in range(minx,maxx+1)]
        hterrains=[None for x in range(minx,maxx+1)]
        #first, ground
        for ct in self._terraingrp:
            if ct.terrainx>=minx and ct.terrainx<=maxx:
                terrains[ct.terrainx-minx]=ct
                hterrains[ct.terrainx-minx]=ct.height
        #now lakes, but only if we crash into them
        if self._lakecrashbehaviour!="extinguish":
            for ct in self._lakegrp:
                if ct.terrainx>=minx and ct.terrainx<=maxx:
                    terrains[ct.terrainx-minx]=ct
                    hterrains[ct.terrainx-minx]=ct.height
        #now work out the angle. 
        try:
            if len(hterrains)>1:
                reg=linregress(list(range(len(hterrains))),hterrains)
                slope=reg.slope       
                #and convert to angle. is opposite/adjacent which is simply slope if adjacent=2
                angle=math.degrees(math.atan(slope)) 
            else:
                angle=0
                slope=0
        except:
            angle=0
            slope=0
        #and return it
        return angle
        
        
    
    

class terrainelement(pygame.sprite.Sprite):
    """
    Terrain element. THis is a single vertical line element 
    """
    def __init__(self,x,height,colour):
        """
        Initialisation
        
        INPUTS:
        x: our x location
        height: Height of the bar
        colour: Colour of the bar
        """
        super().__init__()
        #now save settings
        self.terrainx=x
        self.height=height
        self.colour=colour


        
    def on_init(self,screen):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        
        OUTPUTS: 
        None
        """
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #we are a rectangle of one width
        self.rect=pygame.Rect(self.terrainx,self.screenheight-self.height,1,self.height)
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        #draw if we are on the screen
        self.rect.x=self.terrainx-screenleft
        if self.terrainx>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            pygame.draw.rect(self._screen,self.colour,self.rect)
                 
          
class lakeelement(pygame.sprite.Sprite):
    """
    Lake element. This is a single vertical line element of lake
    """
    def __init__(self,x,terrainheight,height,colour):
        """
        Initialisation
        
        INPUTS:
        x: our x location
        terrainheight: the height of the terrain we are drawn above        
        height: Height of the lake (top)
        colour: Colour of the bar
        """
        super().__init__()
        #now save settings
        self.terrainx=x
        self.height=height
        self.terrainheight=terrainheight
        self.colour=colour
        
    def on_init(self,screen):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        
        OUTPUTS: 
        None
        """
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #we are a rectangle of one width. go one further down to ensure we meet land
        self.rect=pygame.Rect(self.terrainx,self.screenheight-self.height-self.terrainheight+1,1,self.height)
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        #draw if we are on the screen
        self.rect.x=self.terrainx-screenleft
        if self.terrainx>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            pygame.draw.rect(self._screen,self.colour,self.rect)
          

class house(pygame.sprite.Sprite):
    """
    House. This is a single house/tree/etc to draw
    """
    def __init__(self,image):
        """
        Initialisation
        
        INPUTS:
        image: image surface to display
        """
        super().__init__()
        self.image=image
        
        
    def on_init(self,screen,x,y):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        x: our x location
        y: our y location (height form bottom)
        
        OUTPUTS: 
        None
        """
        #now save settings
        self.terrainx=x
        self.terrainy=y
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #and our rect
        self.rect=self.image.get_rect()
        self.rect.bottomleft=(self.terrainx,self.screenheight-self.terrainy)
        #and save width
        self.width=self.rect.width
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        self.rect.x=self.terrainx-screenleft
        if (self.terrainx+self.width)>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            self._screen.blit(self.image, self.rect)
            