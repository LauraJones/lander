"""
Head up display 

Shows critical stats on the display ilcuding
 - fuel bar
 - direction of destination and destinaition indicator
 - time
 - FPS
 
"""


import pygame
from pygame.locals import *
import os.path
import cmath
import math
from scipy.stats import linregress
import pygame_gui

textheight=12

class hud(pygame.sprite.Sprite):
    """
    headup display 
    """
    def __init__(self,options,settings):
        """
        Initialisation 
        
        INPUTS:
        options: options (things like which image sprite to use)
        settings: settings around game type/difficulty (things like gravity)
        """
        #save options
        self._options=options
        self._settings=settings
        self._fuelgaugeheight=options["fuelgaugeheight"]
        self._fuelgaugewidth=options["fuelgaugewidth"]
        self._fuelgaugex=options["fuelgaugex"]
        self._fuelgaugey=options["fuelgaugey"]
        self._fuelgaugecolour=options["fuelgaugecolour"]
        self._fuelgaugelowcolour=options["fuelgaugelowcolour"]
        self._fuelgaugelowpercent=options["fuelgaugelowpercent"]
        self._minshowdistpointer=options["minshowdistpointer"]
        self._showfps=options["showfps"] 
        self.message=None
        #init super
        super().__init__()
        
    def on_init(self,screen,player,landingpads,clock,manager):
        """
        Initialisation. Draw the HUD
        INPUTS:
        screen: the screen to draw on
        player: the player object (to read things like fuel from)
        landingpads: group of landing pads
        clock: The clock object (to get FPS)
        manager: The pygame_gui manager
        
        OUTPUTS:
        None
        """
        #save params
        self._player=player        
        self._landingpads=landingpads
        self._screen=screen
        self._manager=manager
        self._font=pygame.font.Font('tintin.ttf', 12)
        self._bigfont=pygame.font.Font('tintin.ttf', 24)
        self._fueltextrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight,10,20)
        self._veltextrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight,10,20)
        self._disttextrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight*2,10,20)
        #load images
        self._leftimg=pygame.image.load(os.path.join("images","left.png")).convert_alpha()
        self._rightimg=pygame.image.load(os.path.join("images","right.png")).convert_alpha()
        arrowsize=self._rightimg.get_rect().size
        #and a single rect we blit the image to
        self._dirrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight*3,arrowsize[0],arrowsize[1])
        #now the landing pad indicator
        #load image
        self._pointer=pygame.image.load(os.path.join("images","pointer.png")).convert_alpha()
        self._pointersize=self._pointer.get_rect().size
        #now score
        self._scorerect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight*4,10,20)
        #now look for a time in the object
        try:
            stime=self._player.remainingtime
            self._hastime=True
            self._timetextrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight*5,10,20)
        except:
            self._hastime=False
        
        #and check if we want to see fps
        if self._showfps:
            self._showfpsrect=pygame.Rect(self._fuelgaugex,self._fuelgaugey+self._fuelgaugeheight+textheight*6,10,20)
            self._clock=clock


    def update(self,screenleft):
        """
        Update method. blits ourselves onto the screen
        
        INPUTS:
        screenleft: The left side of the screen
        
        OUTPUTS: 
        None
        """
        #draw ourselves
        #fuel gauge is a vertical box on the left side of the sceeen.
        self._gauge=pygame.Rect(
            self._fuelgaugex,self._fuelgaugey+(1-self._player.fuelpc)*self._fuelgaugeheight,
            self._fuelgaugewidth,self._player.fuelpc*self._fuelgaugeheight
            )
        #gauge
        if self._player.fuelpc<self._fuelgaugelowpercent:
            pygame.draw.rect(self._screen,self._fuelgaugelowcolour,self._gauge)
            fueltxt=self._font.render("Fuel: {:3.1f}%".format(self._player.fuelpc*100),False,self._fuelgaugelowcolour)
        else:
            pygame.draw.rect(self._screen,self._fuelgaugecolour,self._gauge)
            fueltxt=self._font.render("Fuel: {:3.1f}%".format(self._player.fuelpc*100),False,self._fuelgaugecolour)
        #and text
        #blit the fuel text on
        self._screen.blit(fueltxt,self._fueltextrect)
        
        #now velocity
        if self._player.safeland():
            veltxt=self._font.render("Vel: {:2.1f}m/s".format(self._player.absvelocity),False,self._fuelgaugecolour)
        else:
            veltxt=self._font.render("Vel: {:2.1f}m/s".format(self._player.absvelocity),False,self._fuelgaugelowcolour)
        self._screen.blit(veltxt,self._veltextrect)
        
        #and distance to pad and indicator
        self._indicatorlocations=[]
        #nad player
        playerx=self._player.coords[0]
        for pad in self._landingpads:
            if pad.destination:
               x=pad.terrainx+pad.width/2
               y=pad.rect.y
               self._indicatorlocations.append([x,y,x-playerx])
 
        #and min distance
        dist=min([abs(c[2]) for c in self._indicatorlocations])
        #and get actual distance
        dist=[c for c in self._indicatorlocations if abs(c[2])==dist][0]
        #now text
        disttxt=self._font.render("Dist: {:3.0f}m".format(abs(dist[2])),False,self._fuelgaugecolour)
        self._screen.blit(disttxt,self._disttextrect)
        #and put arrow on
        if dist[2]<0:
            #less than 0 indicates landing pad is to the left
            img=self._leftimg
        else:
            img=self._rightimg
        #and put it on
        self._screen.blit(img,self._dirrect)
        #now score
        scoretxt=self._font.render("Score: {:4.0f}".format(self._player.score),False,self._fuelgaugecolour)
        self._screen.blit(scoretxt,self._scorerect)
        
        #and indicators
        for indicator in self._indicatorlocations:
            if (abs(indicator[2])>self._minshowdistpointer):
                #now an arrow on the edge of the sceen where the target is, movign to pad at right time
                #we draw a line between the ship and the pad if the pad is off-screen. On the pad if it is onscreen
                if indicator[0]>screenleft and indicator[0]<(screenleft+self._screen.get_width()):
                    #onscreen
                    indx=indicator[0]-screenleft
                    indy=indicator[1]
                    angle=0
                    side=0  #centre
                elif indicator[0]<screenleft:
                    #off left of screen
                    indx=0
                    #distance to where it needs ot be at pad height (when it appears on screen)
                    dx=abs(indicator[2])-self._screen.get_width()/2
                    #now height ratio
                    hr=dx/abs(indicator[2])
                    #now height (remember needs to be from top of screen)
                    dh=(indicator[1]-self._player.coords[1])*hr #how far above pad
                    indy=indicator[1]-dh
                    #and angle 
                    angle=-math.degrees(math.atan(abs(dx/dh)))
                    #if we are below we make angle reflect around 90
                    if dh<0:
                        da=angle+90
                        angle=-(90+da)
                    side=1  #left
                else:
                    #off right
                    indx=self._screen.get_width()
                    #distance to where it needs ot be at pad height (when it appears on screen)
                    dx=abs(indicator[2])-self._screen.get_width()/2
                    #now height ratio
                    hr=dx/abs(indicator[2])
                    #now height (remember needs to be from top of screen)
                    dh=(indicator[1]-self._player.coords[1])*hr #how far above pad
                    indy=indicator[1]-dh
                    #and angle 
                    angle=math.degrees(math.atan(abs(dx/dh)))
                    if dh<0:
                        da=angle-90
                        angle=90-da
                    side=-1  #right
                #now put the indcator on
                rect=pygame.Rect(0,0,self._pointersize[0],self._pointersize[1])
                if side==0:
                    rect.midbottom=[indx,indy]
                elif side==1:
                    rect.bottomleft=[indx,indy]
                elif side==-1:
                    rect.bottomright=[indx,indy]
                #and rotate the image
                img=pygame.transform.rotate(self._pointer,angle)
                self._screen.blit(img,rect)
        
        #time
        if self._hastime:
            #red for low time
            rt=round(self._player.remainingtime)
            if rt<10:
                col=self._fuelgaugelowcolour
            else:
                col=self._fuelgaugecolour
            #need to format time nicely
            timetext_str="{:d}:{:02d}".format(math.floor(rt/60),rt%60)
            timetext=self._font.render(timetext_str,False,col)
            #and put it on the rect
            self._screen.blit(timetext,self._timetextrect)
        
        #FPS
        if self._showfps:
            fpstxt=self._font.render("FPS: {:3.1f}".format(self._clock.get_fps()),False,self._fuelgaugecolour)
            self._screen.blit(fpstxt,self._showfpsrect)
    
        
    def hidemessage(self):
        """
        Hide message
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self.message=None
    
    
    def loop(self):
        """
        Loop, draw boxes etc (tobe blitted later)
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        pass
            

        
    
        