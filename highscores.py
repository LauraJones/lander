"""
Highscores.py
Saves and ranks high scores
Scores are saved in a database. DB schema:
 - each game type gets a table
 - each table has columns:
    - date/time 
    - name
    - score
    - whatever else the save functio gets in the dict
"""

import dataset
import datetime
import json

class highscores:
    """
    Highscores class. Defines high score API.
    
    """
    def __init__(self,settings):
        """
        Initlailisation
        Take in settings, open db, and wait.
        
        INPUTS:
        settings:   Settings
        
        OUPUTS
        None
        """
        dbstring=settings["db"]
        #and open it
        self._db=dataset.connect(dbstring)
    
    def creategametype(self,mode,hscols,rename):
        """
        Create a game type. This tells a few things:
          - Which columns to put in highscore table
        
        INPUTS:
        mode:  Mode of game type
        hscols: Columns to pout in highscore table (JSON list)
        rename: Columns to rename (JSON dict)
        
        OUPUTS
        None
        """
        #get table
        table=self._db.create_table(
            "types",
            primary_id="mode",
            primary_type=self._db.types.text
            )
        #update this entry
        table.upsert({
            "mode":mode,
            "hscols":hscols,
            "rename":rename
            },["mode"])
        
    def save(self,mode,score,name,params):
        """
        Save a score. 
        
        INPUTS:
        mode:   String of game mode (table)
        score:  The score
        name:   Player name
        parms:  Dict of other settings to save
        
        OUTPUTS:
        None
        """
        #get the table
        table=self._db[mode]
        #save the entry
        wd=dict(score=score,name=name,datetime=datetime.datetime.now())
        wd={**wd,**params}
        table.insert(wd,ensure=True)
        
    def rank(self,mode,score,name=None):
        """
        Get the rank of the provided score
        
        INPUTS:
        mode: game mode
        score: score to check
        name: Player name to look for (None for all)
        
        OUTPUTS:
        rank (integer), total (integer)
        """
        #get the data
        try:
            if name is None:
                higher_l=self._db.query("SELECT COUNT(score) FROM {} WHERE score>{}".format(mode,score))
                all_l=self._db.query("SELECT COUNT(score) FROM {}".format(mode,score))
            else:
                higher_l=self._db.query("SELECT COUNT(score) FROM {} WHERE score>{} AND name='{}'".format(mode,score,name))
                all_l=self._db.query("SELECT COUNT(score) FROM {} WHERE name='{}'".format(mode,name))
            higher=-1 #will be 0 if nonexistent
            for h in higher_l:
                higher=h["COUNT(score)"]
            higher+=1 #so that the first isnt '0th'
            all=0
            for a in all_l:
                all=a["COUNT(score)"]
        except:
            print("error getting numbers")
            higher=0
            all=0
        return higher,all
        
    def gametypes(self):
        """
        Returns a list of game types
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #just need to return list of tables, sans "types"
        tbl=self._db.tables
        return [t for t in tbl if t!="types"]
        
    def names(self,mode):
        """
        Return a list of names which have scores for the desired mode
        
        INPUTS:
        mode: Mode to check
        
        OUTPUTS:
        List of names
        """
        #query
        data=self._db.query("SELECT DISTINCT name FROM {}".format(mode))
        #create list
        return [d["name"] for d in data]
        
    def scores(self,mode,max=None,name=None):
        """
        Returns a list of socres.
        
        INPUTS:
        mode:   Game mode
        max:    Maximum number of scores to return
        name:   Name to return
        
        OUTPUTS:
        None
        """
        #first, get columns if the row exists
        types=self._db["types"]
        res=types.find_one(mode=mode)
        if res is None:
            cols="score,name,datetime"
            rename={"datetime":"Date"}
        else:
            cols=",".join(json.loads(res["hscols"]))
            rename=json.loads(res["rename"])
        #read the data
        if name is None:
            ns=""
        else:
            ns=" WHERE name='{}'".format(name)
        if max is None:
            data=self._db.query(
                "SELECT {} FROM {} {} ORDER BY score DESC".format(
                    cols,mode,ns))
        else:
            data=self._db.query(
                "SELECT {} FROM {} {} ORDER BY score DESC LIMIT {} ".format(
                    cols,mode,ns,max))
        #now sort the results
        scores=[]
        for d in data:
            #loop through renames
            for on,nn in rename.items():
                #datetime is specific
                if on=="datetime":
                    #fix datetime
                    dt=datetime.datetime.strptime(d[on],"%Y-%m-%d %H:%M:%S.%f")
                    d[nn]=dt
                    del(d[on])
                else:
                    d[nn]=d[on]
                    del(d[on])
            scores.append(d)
        return scores
        