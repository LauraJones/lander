#dictionary update -> nested from https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
import collections.abc
def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d
    
def update2(dest,src):
    #update only items in dest
    for ky,val in src.items():
        if isinstance(val, collections.abc.Mapping) and ky in dest.keys():
            #it's in dest and its a dict, update
            dest[ky]=update2(dest[ky],val)
        elif ky in dest.keys():
            #is in there, update
            dest[ky]=val
    return dest
    
def findindict(dct,rkey):
    """
    Find a key in the dict and return it's value. Recursive
    
    INPUTS:
    dct: the dict
    rkey: the key
    
    OUTPUTS:
    The key's value
    """
    itemval=None
    for ky,val in dct.items():
        if ky==rkey:
            return val
        if isinstance(val, collections.abc.Mapping):
            #it's in dest and its a dict, update
            rv=findindict(val,rkey)
            if not rv is None:
                return rv
    return None
    
if __name__=="__main__":
    x={"eggs":55,"cheese":{"Fff":55},"nose":"b"}
    print("Cheese: {}".format(findindict(x,"cheese")))
    print("Eggs: {}".format(findindict(x,"eggs")))
    print("spool: {}".format(findindict(x,"spool")))
    x[55]="b"
    print("Cheese: {}".format(findindict(x,"cheese")))
    print("55: {}".format(findindict(x,55)))
    print("Fff: {}".format(findindict(x,"Fff")))
    print("X: {}".format(x))