# Lunar Lander game #

This is a simple implementation of a lunar lander game in Python/pygame. 
![Main Menu](images/screenshots/main.png)

## Features ##

Lander Game is a simple impelmentation of the classic "lunder lander" game of the 80s. 
The player controls a ship, landing it on a landing pad without running out of fuel. 

## Playing the game

### Controls

The controls for this game are:

- Space fires the engine
- Left/right arrow keys rotate the ship

### Settings

There are several user-configurable settings in lander game. 
These can be changed through the "settings" main menu control.

![Settings](images/screenshots/settings.png) 

 - Game Mode: Select the desired game mode
 - Gravity: Changes how quickly the ship accelerates toward the bottom of the screen
 - Power/Weight ratio: Changes how powrful the ships engine is relative to it's weight. A higher number means the ship will accelerate faster
 - Fuel: The starting fuel. The more fuel, the easier
 - Auto Straighten: If set, the ship will automatically go back to vertical when no rotate keys are pressed
 - Map width: The width of the map. Wider maps may be slow 
 - World: Which world type to play on

There are four world types: Mars, the Moon, Europa, and Titan

![Worlds](images/screenshots/worlds.png)

### Game modes

There are two game modes:

 - Lander
 - Package delivery

Lander mode is the classic lander game. The aim is to land the ship a fast and with as much fuel as possible.
Points for this mode are based on how fast and how efficiently the ship lands. 

Package delivery involves picking up and delivering packages as fast as possible. Pints are awarded for each package delivered.
The game is over when you run out of time or crash.

# Installation #
To install:

1. Clone or download the repository. 
2. Requirements can be installed from requirements.txt using pip:

 ```
 pip install -r requirements.txt
 ```

Lander has been built in python 3.9.7.
 
## Screenshots ##

![Game play 1](images/screenshots/gameplay1.png)
![Game play 2](images/screenshots/gameplay2.png)
![Game play 3](images/screenshots/gameplay3.png)
![Game play 4](images/screenshots/gameplay4.png)
 



