"""
Lader game: the basic lunar lander implementation
Land rocket as fast and with as much fuel as possible
"""


import pygame
from pygame.locals import *
import os.path
import cmath
import math
from scipy.stats import linregress
import random
import time
import pygame_gui
import json
import baseplayer

class Rocket(baseplayer.baseplayer):
    """
    Rocket class is the player in he game
    draws a rocket, effected by gravity. Crashes if lands on terrain, or lands if lands on landing pad slowly enough
    """
    def __init__(self,options,settings,highscores,game):
        """
        Initalisation. saves parameters and sets initial ship state
        
        INPUTS:
        options: options (things like which image sprite to use, gravity, etc)
        settings: settings
        highscores: highscores object
        game: game object
        """
        super().__init__(options,settings,highscores,game)
        #save options
        #highscores
        self._highscores=highscores
        #and set up table
        self._highscores.creategametype(
            "lander",
            json.dumps(["name","score","fuelscore","distscore","datetime"]),
            json.dumps({"fuelscore":"Efficiency","distscore":"Speed","datetime":"Date"})
            )
        #scoring factors
        self._scoringfactors=options["scoring"]

    
    def on_init(self,screen,x,y,manager):
        """
        Set up graphics, read image files etc
        
        INPUTS:
        screen: The screen to draw on
        x: Starting x location
        y: Starting y location
        manager: GUI manager for window
        
        OUTPUTS: 
        None
        """
        #init superclass
        super().on_init(screen,x,y,manager)
        #now bases (list of images)
        bases=self._terrainoptions["baseimages"]
        self._baseimgs=[]
        for base in bases:
            #path
            fpath=os.path.join("images",base)
            #load file
            self._baseimgs.append(pygame.image.load(fpath).convert_alpha())
        #show welcome
        self.showwelcome()

     
    def loop(self):
        """
        Loop. call main class and then check for land/crash
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        super().loop()
        self.landingpads()
        self.terrain()
   
    def landingpads(self):
        """
        Assess landing pads to see if we have landed
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.crashed:
            return
        if self.rect.colliderect(self._landingpad.rect):
            #landed/crashed
            if self.safeland():
                self.land()
            else:
                self.angle=0
                self.crash()
            return
        #not landed
        self.landed=False
                                  
    def land(self):
        """
        Land on a landing pad. we also want to do scoring if its first land event
        
        INPUTS:
        pady: the landing pad height
        
        OUTPUTS:
        None
        """
        if self.landed:
            return
        #set landed flag
        self.landed=True
        #and calcualte score
        #scoring vars
        if not self._scored:
            #now calculate scores
            self.score,self._scoredict=self.calculatescore()           
            self._scored=True #score on first landing. prevents mroe scoring         
            #now show the message
            self._dlg=baseplayer.dialogue(
                True,self.score,self._scoredict,self._highscores,self._screen,self._manager,self.exit,"lander"
                )
        #and set velocity, angle to zero
        self._vx=0
        self._vy=0
        self.angle=0
    
    def calculatescore(self):
        """
        Calculate the score 
        
        INPUTS:
        None
        
        OUTPUTS:
        score, scoring parameters
        """
        if self._scored:
            #avoid recalcs
            return self.score,self._scoredict
        #scores are based on a few metrics: 
        # - fuel efficiency (fuel used per unit of horizontal distance)
        # - average speed (time per unit of horizontal distance)
        #there are several factors that can increase/decrease a score, applied as factors at the end:
        # - Gravity
        # - auto straighten
        # - power/weight ratio
        #get distance, base for other calcs
        dist=((self._initlocation[0]-self.coords[0])**2+(self._initlocation[1]-self.coords[1])**2)**0.5
        #return if it's zero, or we havent cosumed any fuel
        if dist==0 or self.initfuel==self.fuel:
            return 0,{}
        
        #first efficiency (or essentially the inverse, we want blocks/unit not units/block). a bigger number (more blocks/unit) is better
        fueleff=dist/(self.initfuel-self.fuel)
        #and average speed. in blocks/second  
        speed=dist/(time.time()-self._starttime-self._pausedtime)
        #now score take 1
        fuelscore=fueleff*self._scoringfactors["fuel"]
        distscore=speed*self._scoringfactors["distance"]
        score=fuelscore+distscore
        
        #now the factors
        #first autostraighten. this is simply boolean 
        sf=1
        if self._autostraighten:
            sf=self._scoringfactors["straighten"]
            score=score*sf
        #now for gravity. Gravity centres on 1 at centregravity
        #on the high end it reaches highgravityval at higgravity
        #on the low end it reaches lowgravityval at lowgravity
        if self._gravity>self._scoringfactors["centregravity"]:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["highgravity"]-self._scoringfactors["centregravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["highgravityval"]-1)+1
        else:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["centregravity"]-self._scoringfactors["lowgravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["lowgravityval"]-1)+1
        score=score*gfactor
        #now power/weight ratio
        #higher is obviously easier. We make it linear between 1 and 3, where 1 is powerweightmult and 5 is 1
        pwratio=self._thrusterforce/(self._mass*self._gravity)
        slope=-(self._scoringfactors["powerweightmult"]-1)/2
        pwratiofactor=max(
            self._scoringfactors["minpowerweightmult"],
            self._scoringfactors["powerweightmult"]+pwratio*slope
            )
        #save the params in a dict for later
        dct=dict(
            fuelrem=self.fuelpc,
            dist=dist,
            straighen=self._autostraighten,
            gravity=self._gravity,
            powerweight=pwratio,
            fueleff=fueleff,
            speed=speed,
            fuelscore=fuelscore,
            distscore=distscore,
            straightenfactor=sf,
            gravityfactor=gfactor,
            pwratiofactor=pwratiofactor
            )
        return score*pwratiofactor,dct
                

        
    def safeland(self):
        """
        Determine if a landing is safe or should result in a crash
        
        INPUTS:
        None
        
        OUTPUTS:
        bool (true for safe landing)
        """
        return math.sqrt(self._vx**2+self._vy**2)<=self._maxlandingvelocity
        
        
    def addlandingpads(self,terrain):
        """
        Add landing pads and related scenery to map
        Called immediately after hightmap is made
        
        INPUTS:
        terrain: terrain object
        
        OUTPUTS:
        heightmap and list of sprites
        """
        #save terrian object
        self._terrain=terrain
        #landing pad is placed at the flattest spot on the map
        padwidth=self._terrainoptions["padwidth"]
        #first, create a landing pad
        landingpad_o=baseplayer.landingpad(3,padwidth,[0,255,0])
        #now find the best spot
        best,selector=terrain.findarea(padwidth,landingpad_o.suitabilitychecker)
        #lets not worry about None here, as there should be somwhwere
        #and place
        height=terrain.createfoundation(best,padwidth)
        landingpad_o.on_init(self._screen,best,height)
        #and save for later
        self._landingpad=landingpad_o
        
        #now the base this is located near the pad (within 4 pad sizes)
        #first, randomly select a pad
        baseimg=random.choice(self._baseimgs)
        basewidth=baseimg.get_width()
        #and the credible areas
        base_o=baseplayer.base(baseimg)
        basex,leftselector=terrain.findarea(basewidth,base_o.suitabilitychecker,best-basewidth*3,best+padwidth+basewidth*3)
        if not basex is None:
            #was n area, place
            #flatten area
            basey=terrain.createfoundation(basex,basewidth)
            #and place the base
            base_o.on_init(self._screen,basex,basey)       
            #and return
            return [landingpad_o],[base_o]
        else:
            #no spot to put base, so just no base
            return [landingpad_o],[]
            
    
    def exit(self):
        """
        Exit the game
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        self._game.on_exit()
        
    def crash(self):
        """
        Crash the ship. Sessentially jst extends base to enable scoring
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #super
        super().crash()        
        #lost dialogue
        self._dlg=baseplayer.dialogue(
            False,0,{},self._highscores,self._screen,self._manager,self.exit,"lander"
            )
    