"""
Lunar lander game.
This is a simple lunar landar type game. Players must land their spaceships on a randomly generated surface without
running out of fuel. 
This code creates the game world and manages calling of other elements. Components are:
 - Player (rocket)
    Player is the ship under the user's control. It also manages game rules
 - Terrain:
    Terrain is the ground. Most terrain destroys the ship if touched, but landing pads are also terrain
 - HUD:
    HUD shows stats like fuel level, score, etc
The screen can be narrower than the world, side scrolling. 

TODO:
 - description of game mode to be shown in settings (done) and start of game (done)
 - Main screen to show game mode (done)
 - Improve score gui: 
    - take in an elements dict (descriptions) to show
    - take in a text string to describe the condition
 - Show score in HUD (done)
 - smoke (done)
 - better readme
 - sound
 - fix path (done)
 - Packages on landing pad for package delivery (done)
 - Lander mode levels -> time decreases each level (done)
 - Settings per game mode (done)
 
"""

import pygame
from pygame.locals import *
import pygame.font
import yaml
import json
#from delivery import Rocket #delivery game
from terrain import terrain
from hud import hud
import time
import os.path
import pygame_gui
import random
import highscores
import math
import numbers
import datetime
from pathlib import Path
import collections.abc


#some functions
#dictionary update -> nested. 
#adapted from https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
def updatedict(dest,src):
    """
    Update dictionary, but only items that exist in dest already
    Recursive.
    
    INPUTS:
    dest: Destination dict (is updated)
    src: Source dict
    
    OUTPUTS:
    dest dict
    """
    #update only items in dest
    for ky,val in src.items():
        if isinstance(val, collections.abc.Mapping) and ky in dest.keys():
            #it's in dest and its a dict, update
            dest[ky]=updatedict(dest[ky],val)
        elif ky in dest.keys():
            #is in there, update
            dest[ky]=val
    return dest

def findindict(dct,rkey):
    """
    Find a key in the dict and return it's value. Recursive
    
    INPUTS:
    dct: the dict
    rkey: the key
    
    OUTPUTS:
    The key's value
    """
    itemval=None
    for ky,val in dct.items():
        if ky==rkey:
            return val
        if isinstance(val, collections.abc.Mapping):
            #it's in dest and its a dict, update
            rv=findindict(val,rkey)
            if not rv is None:
                return rv
    return None
    
class Game:
    """
    Lander app class. This shows the game window, manages sprite drawing, etc.
    A world consists of: a player, terrain, scenery, and a HUD. 
    The general API for these things is:
     - on_init: initialise. Is supplied with window at this point
     - update: draw ourselves onto the screen
     - event (player, HUD, game): respond to events  (keypreses)
     - loop: (player, HUD only): Loop (physics, update display, etc)
    """
    def __init__(self,settings,gamemode,surf=None):
        """
        Initlalisation. 
        Save parameters
        
        INPUTS:
        settings: Settings dictionary (all settings, but also for ourselves)
        gamemode: dict of the game mode settings
        surf: Display surface       
        """
        #save game settings
        self._settings=settings
        self._gamemode=gamemode
        #read screen size
        self._size = self._width, self._height = self._settings["game"]["screenwidth"], self._settings["game"]["screenheight"]
        #and map width
        self._mapwidth=self._settings["game"]["mapwidth"]
        #backgorund colour
        self.bgcolour=self._settings["game"]["bgcolour"]
        #set flags
        self._running = False
        #and display surface
        self._display_surf = surf
        #background colour. maybe a setting later 
        #Groups - we have groups for:
        # - Player (RenderUpdates)
        # - Ground (RenderDraw)
        # - Background (RenderDraw)
        # - Foreground (RenderDraw)
        self._playergroup = pygame.sprite.RenderUpdates()
        self._terrain = None
        self._hudgroup = pygame.sprite.RenderUpdates()
        #and gui manager for button
        self._manager=pygame_gui.UIManager(self._size,"theme.json")
        #default to exiting on on_cleanup
        self._exit=True
        
    
    def on_init(self):
        """
        Initialisation script. Runs to init, start up screen, etc
        Creates player, terrain, hud and the like
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #create clock
        self._clock = pygame.time.Clock()
        #set diplay surface. create if we werent supplied one
        if self._display_surf is None:
            self._display_surf = pygame.display.set_mode(self._size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        #fill it
        self._display_surf.fill(self.bgcolour)
        
        #now create high scores object
        self._highscores=highscores.highscores(settings["score"])
        
        #now create sprites
        #player
        #attempt to import the right module
        try:
            mod=__import__(self._gamemode["module"])
        except:
            print("Error importing game mode {} module: {}".format(self._gamemode["name"],self._gamemode["module"]))
            raise
        self._player=mod.Rocket(self._gamemode,self._settings["game"],self._highscores,self)
        #start in top middle
        self._player.on_init(self._display_surf,self._mapwidth/2,100,self._manager)
        #add to list
        self._playergroup.add(self._player)
        
        #and terrain
        self._terrain=terrain(self._settings["terrain"],self._settings["game"])
        self._terraingrp, self._padgrp, self._scenerygrp=self._terrain.on_init(
            self._display_surf,self._player
            )
        
        #and hud
        chud=hud(self._settings["hud"],self._settings["game"])
        chud.on_init(self._display_surf,self._player,self._padgrp,self._clock,self._manager)
        self._hudgroup.add(chud)
        
        #now where we are on the screen.
        self._visleft=self._mapwidth/2-self._width/2
        
        #now show a back button
        self._backbutton=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self._width-120, 20), (100, 40)),
             text='Back',
             manager=self._manager,
             visible=True)
        
        #running flag
        self._running = True
        return True
        
    def on_event(self, event):
        """
        Event handler. all things that have events (keypresses and the like) are here
        
        INPUTS:
        event: the event to process
        
        OUTPUTS:
        None
        """
        #first, exit if the event type is quit
        if event.type == pygame.QUIT:
            self._running = False
        #buttons
        if event.type==pygame.USEREVENT:
            if event.user_type==pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element==self._backbutton:
                    self.on_exit()
                else:
                    #pass to game
                    self._player.on_event(event)
        #now pass to player
        for ship in self._playergroup.sprites():
            ship.event(event)
        #process GUI events
        self._manager.process_events(event)
        
            
    def on_loop(self):
        """
        Items that have loop methods go here. Also looks for crashes, landings etc
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #first, the ships. these have physics so need a loop.
        for ship in self._playergroup.sprites():
            ship.loop()
        #and hud
        for chud in self._hudgroup.sprites():
            chud.loop()
        #now get x locs for positoning screen. do now in case collisions repositioned
        xlocs=[]
        for ship in self._playergroup.sprites():
            ship.loop()
            xlocs.append(ship.coords[0])
        #centre screen over avereage
        avgx=sum(xlocs)/len(xlocs)
        #and set left visible
        self._visleft=min([max([0,avgx-self._width/2]),self._mapwidth-self._width])
        
        
    def on_render(self):
        """
        Update screen. Calls all the group update methods and shows the screen
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #blank screen
        self._display_surf.fill(self.bgcolour)
        #update groups
        self._terrain.update_background(self._visleft)
        self._padgrp.update(self._visleft)
        self._playergroup.update(self._visleft)
        
        #HUD is always the same place, but some elements need left of screen
        self._hudgroup.update(self._visleft)
        
        #and draw ui
        self._manager.draw_ui(self._display_surf)
        #Refresh Screen
        pygame.display.flip()
        
    def on_exit(self):
        """
        Exit the game
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #hide the button
        self._backbutton.hide()
        #flags
        self._running=False
        self._exit=False
        
    def on_cleanup(self):
        """
        When we want to quit
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self._exit:
            pygame.quit()
            return False
        else:
            return True
 
    def on_execute(self):
        """
        Run the game. this contains the main loop
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            #events
            for event in pygame.event.get():
                self.on_event(event)
            #loop, update() functions
            self.on_loop()
            #update/render
            self.on_render()
            #Number of frames per secong e.g. 60
            td=self._clock.tick(60)/1000
            self._manager.update(td)
        return self.on_cleanup()
        
    def redraw_terrain(self,randomise=False):
        """
        Builds new terrain 
        
        INPUTS:
        randomise: True to randomise the planet
        
        OUTPUTS:
        None
        """
        #randomly select a terrain if needed
        if randomise:
            worldname=random.choice(list(self._settings["terrain"]["worlds"].keys()))
            print(worldname)
            world=self._settings["terrain"]["worlds"][worldname]
            self._settings["terrain"].update(world)
        
        #Create terrain
        self._terrain=terrain(self._settings["terrain"],self._settings["game"])
        self._terraingrp, self._padgrp, self._scenerygrp=self._terrain.on_init(
            self._display_surf,self._player
            )
        
        #and hud
        for s in self._hudgroup:
            s.on_init(self._display_surf,self._player,self._padgrp,self._clock,self._manager)
        
        #now where we are on the screen.
        self._visleft=self._mapwidth/2-self._width/2
        
        #running flag
        self._running = True
        return True
        
        
class MainMenu:
    """
    Main menu app. Shows the main menu of the lander game
    The general API for these things is:
     - on_init: initialise. Is supplied with window at this point
     - update: draw ourselves onto the screen
     - event (player, HUD, game): respond to events  (keypreses)
     - loop: (player, HUD only): Loop (physics, update display, etc)
    """
    def __init__(self,settings,surf=None):
        """
        Initlalisation. 
        Save parameters
        
        INPUTS:
        settings: Settings dictionary (all settings, but also for ourselves)
        """
        #save game settings
        self._settings=settings
        #read screen size
        self._size = self._width, self._height = self._settings["game"]["screenwidth"], self._settings["game"]["screenheight"]                
        #now work out a good left side of the screen for back buttons
        self.leftscreen=self._width-150
        
        #set flags
        self._running = False
        #and display surface
        self._display_surf = surf
        
        #and power/weight ratio (calculated)
        self._powerweight=settings["game"]["thrusterforce"]/(settings["game"]["shipmass"]*settings["game"]["gravity"])
        #and world
        self.world="Random"
        
    
    def on_init(self):
        """
        Initialisation script. Runs to init, start up screen, etc
        Creates player, terrain, hud and the like
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #create clock
        self._clock = pygame.time.Clock()
        #set diplay surface. create if we werent supplied one
        if self._display_surf is None:
            self._display_surf = pygame.display.set_mode(self._size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        #and pygame_gui manager
        self._manager=pygame_gui.UIManager(self._size,"theme.json")
        #rect for backrond image always covers whole screen
        self.rect=pygame.Rect(0,0,self._width,self._height)
        
        #now game modes. look for settings groups that start with "mode"
        self._modes={
            self._settings[g]["name"]:self._settings[g] 
            for g in self._settings.keys() if g.startswith("mode")
            }
        #and current game mode is always "lander"
        self._c_gtype="Lander"
        
        #and now show menu
        
        ###########################################################
        #main menu
        ###########################################################
        #buttons
        self._mm_buttons={}
        #play button
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((700, 450), (150, 40)),
             text='Play',
             manager=self._manager,
             visible=False)
        self._mm_buttons["play"]=b
        #settings
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((700, 500), (150, 40)),
             text='Settings',
             manager=self._manager,
             visible=False) 
        self._mm_buttons["settings"]=b
        #highscores
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((700, 550), (150, 40)),
             text='High scores',
             manager=self._manager,
             visible=False) 
        self._mm_buttons["highscores"]=b
        #Credits
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((700, 600), (150, 40)),
             text='Credits',
             manager=self._manager,
             visible=False)  
        self._mm_buttons["credits"]=b
        #Quit
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((700, 650), (150, 40)),
             text='Quit',
             manager=self._manager,
             visible=False)  
        self._mm_buttons["quit"]=b
        #show current game mode
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((860, 435), (150, 40)),
             text="Game Mode:",
             manager=self._manager,
             visible=False) 
        self._mm_buttons["gmode_label"]=b
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((848, 465), (175, 40)),
             text=self._c_gtype,
             manager=self._manager,
             visible=False) 
        self._mm_buttons["gmode"]=b
        #and read splash image
        fpath=os.path.join("images","splash.png")
        self._mm_image=pygame.image.load(fpath).convert_alpha()
        
        ###########################################################
        #Settings menu
        ###########################################################
        #buttons/controls. come in two columns, once centred on 340, one on 680
        self._set_buttons={}
        #and the game mode settings go here
        self._gamemode_settings={}
        #game type. Make this settable from config later
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 150), (200, 40)),
             text="Game Mode",
             manager=self._manager,
             visible=False)  
        self._set_buttons["gametypelabel"]=b
        #dropdown
        b=pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(relative_rect=pygame.Rect((300, 150), (200, 40)),
             options_list=list(self._modes.keys()),
             starting_option='Lander',
             manager=self._manager,
             visible=False)  
        self._set_buttons["gametype"]=b       
        #description box is added at runtime because html formatted doesnt let you update
        self._modedescription=None
        
        #Gravity
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 200), (200, 40)),
             text="Gravity",
             manager=self._manager,
             visible=False)  
        self._set_buttons["gravitylabel"]=b
        #slide
        b=pygame_gui.elements.ui_horizontal_slider.UIHorizontalSlider(relative_rect=pygame.Rect((300, 200), (200, 40)),
             start_value=settings["game"]["gravity"],
             value_range=(0.1,25),
             manager=self._manager,
             visible=False)  
        self._set_buttons["gravity"]=b
        #and labels for current value
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 240), (400, 40)),
             text="-----",
             manager=self._manager,
             visible=False)  
        self._set_buttons["gravitydesc"]=b
        
        #Power/weight ratio
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 300), (200, 40)),
             text="Power/Weight ratio",
             manager=self._manager,
             visible=False)  
        self._set_buttons["pwratiolabel"]=b
        #slider
        b=pygame_gui.elements.ui_horizontal_slider.UIHorizontalSlider(relative_rect=pygame.Rect((300, 300), (200, 40)),
             start_value=self._powerweight,
             value_range=(1.1,5),
             manager=self._manager,
             visible=False)  
        self._set_buttons["pwslider"]=b
        
        #fuel
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 350), (200, 40)),
             text="Fuel :{:.0f}".format(settings["game"]["fuel"]),
             manager=self._manager,
             visible=False)  
        self._set_buttons["fuellabel"]=b
        #slider
        b=pygame_gui.elements.ui_horizontal_slider.UIHorizontalSlider(relative_rect=pygame.Rect((300, 350), (200, 40)),
             start_value=settings["game"]["fuel"],
             value_range=(100,10000),
             manager=self._manager,
             visible=False)  
        self._set_buttons["fuelslider"]=b
        
        #autostraighten
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 400), (200, 40)),
             text="Auto straighten ship",
             manager=self._manager,
             visible=False)  
        self._set_buttons["autostraightenlabel"]=b
        #dropdown
        b=pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(relative_rect=pygame.Rect((300, 400), (200, 40)),
             options_list=["Yes","No"],
             starting_option="Yes" if settings["game"]["autostraighten"] else "No",
             manager=self._manager,
             visible=False)  
        self._set_buttons["autostraighten"]=b
        
        #world width
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 450), (200, 40)),
             text="Map Width :{:.0f}".format(settings["game"]["mapwidth"]),
             manager=self._manager,
             visible=False)  
        self._set_buttons["mapwidthlabel"]=b
        #slider
        b=pygame_gui.elements.ui_horizontal_slider.UIHorizontalSlider(relative_rect=pygame.Rect((300, 450), (200, 40)),
             start_value=settings["game"]["mapwidth"],
             value_range=(settings["game"]["screenwidth"],settings["game"]["screenwidth"]*16),
             manager=self._manager,
             visible=False)  
        self._set_buttons["mapwidthslider"]=b
        
        #World
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((100, 500), (200, 40)),
             text="World",
             manager=self._manager,
             visible=False)  
        self._set_buttons["worldlabel"]=b
        #dropdown
        dl=["Random"]+list(settings["terrain"]["worlds"].keys())
        b=pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(relative_rect=pygame.Rect((300, 500), (200, 40)),
             options_list=dl,
             starting_option="Random",
             manager=self._manager,
             visible=False)  
        self._set_buttons["world"]=b
        
        #Quit
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.leftscreen, 600), (100, 40)),
             text='Back',
             manager=self._manager,
             visible=False)  
        self._set_buttons["back"]=b
        #and read splash image
        fpath=os.path.join("images","settingsscreen.png")
        #and set gravity description
        self._setgetandupdate()
        self._set_image=pygame.image.load(fpath).convert_alpha()
        
        ###########################################################
        #Credits menu
        ###########################################################
        #just need a back button
        self._credits_buttons={}
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.leftscreen, 20), (100, 40)),
             text='Back',
             manager=self._manager,
             visible=False)  
        self._credits_buttons["back"]=b
        #and read splash image
        fpath=os.path.join("images","creditsscreen.png")
        self._credits_image=pygame.image.load(fpath).convert_alpha()
        
        ###########################################################
        #Highscores menu
        ###########################################################
        #now create high scores object
        self._highscores=highscores.highscores(settings["score"])
        #back button
        self._highscores_buttons={}
        b=pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.leftscreen, 20), (100, 40)),
             text='Back',
             manager=self._manager,
             visible=False)  
        self._highscores_buttons["back"]=b
        #now game type drop down
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((20, 100), (125, 40)),
             text="Game type",
             manager=self._manager,
             visible=False)  
        self._highscores_buttons["gametypeslabel"]=b
        #now dropdown
        gametypes=self._highscores.gametypes()
        try:
            default=gametypes[0]
        except:
            default=""
        b=pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(relative_rect=pygame.Rect((145, 100), (150, 40)),
             options_list=gametypes,
             starting_option=default,
             manager=self._manager,
             visible=False)  
        self._highscores_buttons["gametypes"]=b
        self._highscores_selectedgtype=b
        #now player filter
        #label
        b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((350, 100), (125, 40)),
             text="Player",
             manager=self._manager,
             visible=False)  
        self._highscores_buttons["playernamelabel"]=b
        #fill out player names dropdown on runtime
        self._highscores_table_elements=[]
        self._highscores_table_draw=None
        self._highscores_selectedplayer=None
        #and read splash image
        fpath=os.path.join("images","highscores.png")
        self._highscores_image=pygame.image.load(fpath).convert_alpha()
        
        
        #and guistate. States:
        # 1: main menu
        # 2: settings menu
        # 3: Credits
        # 4: Highscores
        #negative numbers signal to redraw the screen
        self._guistate=-1
        
        
        #running flag
        self._running = True
        return True
        
    def on_event(self, event):
        """
        Event handler. all things that have events (keypresses and the like) are here
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #first, exit if the event type is quit
        if event.type == pygame.QUIT:
            self._running = False
        #gui events
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if self._guistate==1:
                    #main menu
                    self._mm_controls(event)
                elif self._guistate==2:
                    #settings menu
                    self._set_controls(event)
                elif self._guistate==3:
                    #credits menu
                    self._credits_controls(event)
                elif self._guistate==4:
                    #highscores menu
                    self._highscores_controls(event)
            elif event.user_type == pygame_gui.UI_HORIZONTAL_SLIDER_MOVED:
                #slid a slider
                if self._guistate==2:
                    #settigs menu
                    self._set_controls(event)
            elif event.user_type == pygame_gui.UI_DROP_DOWN_MENU_CHANGED:
                #changed a dropdown
                if self._guistate==2:
                    #settigs menu
                    self._set_controls(event)
                elif self._guistate==4:
                    #highscores menu
                    self._highscores_controls(event)
        #process GUI events
        self._manager.process_events(event)
        
    def _mm_controls(self,event):
        """
        Controls for main menu (buttons)
        
        INPUTS:
        event: the item that called the event
        
        OUTPUTS:
        None
        """
        #play button
        if event.ui_element == self._mm_buttons["play"]:
            #first, do any settings (e.g. random selections) needed
            self._randomise()
            #now load game settings. 
            #we need to add game mode to default
            gameset=self._settings["allmodes"]
            gameset.update(self._modes[self._c_gtype])
            game=Game(self._settings,gameset,self._display_surf)
            self._running=game.on_execute()
        #settings
        if event.ui_element == self._mm_buttons["settings"]:
            #hide our buttons
            for en,el in self._mm_buttons.items():
                el.hide()
            #and sstate
            self._guistate=-2
            #and load the game settings
            self._loadgametypesettings()
        #credits
        if event.ui_element == self._mm_buttons["credits"]:
            #hide our buttons
            for en,el in self._mm_buttons.items():
                el.hide()
            #and state
            self._guistate=-3
        if event.ui_element == self._mm_buttons["highscores"]:
            #hide our buttons
            for en,el in self._mm_buttons.items():
                el.hide()
            #and sstate
            self._guistate=-4
        #quit button
        if event.ui_element == self._mm_buttons["quit"]:
            self._running=False
            self.on_cleanup()
        
    def _set_controls(self,event):
        """
        Controls for settings menu (buttons)
        
        INPUTS:
        event: the item that called the event
        
        OUTPUTS:
        None
        """
        #back button
        if event.ui_element == self._set_buttons["back"]:
            #hide our buttons
            for en,el in self._set_buttons.items():
                el.hide()
            #and the game description element
            if not self._modedescription is None:
                self._modedescription.kill()
            self._modedescription=None
            #and state
            self._guistate=-1
        elif (event.ui_element == self._set_buttons["gravity"] or 
                event.ui_element == self._set_buttons["pwslider"] or
                event.ui_element == self._set_buttons["fuelslider"] or
                event.ui_element == self._set_buttons["autostraighten"] or 
                event.ui_element == self._set_buttons["mapwidthslider"] or
                event.ui_element == self._set_buttons["gametype"] or
                event.ui_element == self._set_buttons["world"]):
            self._setgetandupdate() 
        
        
    def _credits_controls(self,event):
        """
        Controls for credits screen (buttons)
        
        INPUTS:
        event: the item that called the event
        
        OUTPUTS:
        None
        """
        #back button
        if event.ui_element == self._credits_buttons["back"]:
            #hide our buttons
            for en,el in self._credits_buttons.items():
                el.hide()
            #and state
            self._guistate=-1
            
    def _highscores_controls(self,event):
        """
        Controls for high scores screen (buttons)
        
        INPUTS:
        event: the item that called the event
        
        OUTPUTS:
        None
        """
        #back button
        if event.ui_element == self._highscores_buttons["back"]:
            #hide our buttons
            for en,el in self._highscores_buttons.items():
                el.hide()
            #delete all table elements
            for el in self._highscores_table_elements:
                el.kill()
            self._highscores_table_elements=[]
            self._highscores_selectedplayer.kill()
            self._highscores_selectedplayer=None
            #and state
            self._guistate=-1
        elif event.ui_element == self._highscores_selectedplayer:
            #user selected a new player
            #kill all UI elements
            for el in self._highscores_table_elements:
                el.kill()
            self._highscores_table_elements=[]
            #rebuild table
            self.show_highscores(rebuild=False)
        elif event.ui_element == self._highscores_selectedgtype:
            #user selected a new game type
            #kill all UI elements
            for el in self._highscores_table_elements:
                el.kill()
            self._highscores_table_elements=[]
            #rebuild table
            self.show_highscores(rebuild=False)
            
        
    def _setgetandupdate(self):
        """
        Set the labels and settings for settings screen
        
        INPUTS:
        
        OUTPUTS:
        None
        """
        #if we have changed game mode, need to reload settings
        s_gtype=self._set_buttons["gametype"].selected_option
        if s_gtype!=self._c_gtype:
            self._c_gtype=s_gtype
            self._loadgametypesettings()
        #gravity
        #get the slider value
        cval=self._set_buttons["gravity"].get_current_value()
        #save in settings
        settings["game"]["gravity"]=cval
        self._powerweight=self._set_buttons["pwslider"].get_current_value()
        #and work out thrust. 
        settings["game"]["thrusterforce"]=settings["game"]["shipmass"]*cval*self._powerweight
        #now set the label text for gravity
        labellookup={0.61:"Pluto",1.62:"Moon",3.7:"Mars",8.87:"Uranus",9.8:"Earth",10.445:"Saturn",11.15:"Neptune",24.79:"Jupiter"}
        #source: https://gravity.wikia.org/wiki/List_of_Solar_System_objects_by_radius
        #pick closest
        planet=labellookup.get(cval, labellookup[min(labellookup.keys(), key=lambda k: abs(k-cval))])
        #now put in label
        self._set_buttons["gravitydesc"].set_text("{}:{:.2f}m/s".format(planet,cval))
        
        #now fuel
        settings["game"]["fuel"]=self._set_buttons["fuelslider"].get_current_value()
        #and set label
        self._set_buttons["fuellabel"].set_text("Fuel :{:.0f}".format(settings["game"]["fuel"]))
        
        #and autostraighten
        settings["game"]["autostraighten"]=self._set_buttons["autostraighten"].selected_option=="Yes"
        
        #map width
        settings["game"]["mapwidth"]=self._set_buttons["mapwidthslider"].get_current_value()
        #and set label
        self._set_buttons["mapwidthlabel"].set_text("Map width: {:.0f}".format(settings["game"]["mapwidth"]))
        
        #world
        self.world=self._set_buttons["world"].selected_option
        
    def _loadgametypesettings(self):
        """
        Load game type defaults
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #get the settings
        settings=self._modes[self._c_gtype]["defaultgame"]
        #save them
        self._set_buttons["gravity"].set_current_value(settings["gravity"])
        #ship mass
        self._powerweight=settings["thrusterforce"]/(settings["shipmass"]*settings["gravity"])
        self._set_buttons["pwslider"].set_current_value(self._powerweight)
        self._set_buttons["fuelslider"].set_current_value(settings["fuel"])
        #autostraighten
        self._set_buttons["autostraighten"].selected_option="Yes" if settings["autostraighten"] else "No"
        
        #now description. Is a text box
        #delete old device
        if not self._modedescription is None:
            self._modedescription.kill()
        #get the decrpiption
        desc=self._modes[self._c_gtype]["description"]
        #now create a box
        self._modedescription=pygame_gui.elements.ui_text_box.UITextBox(relative_rect=pygame.Rect((550, 150), (400, 100)),
            html_text=desc,
            manager=self._manager)
        
        #now game mode settings go below the box
        try:
            settables=self._modes[self._c_gtype]["settings"]
        except:
            #obviously were none
            settables={}
        #now make them
        row=1
        print(settables)
        
        for sn, sd in settables.items():
            #First, get the existing setting 
            #this will be a JSON string, but settign is replaced with "__setting__"
            #just replace with any number for now
            setdict=json.loads(sd["setting"].replace("__setting__",'"Placeholder"'))
            #now update with the current setting
            updatedict(setdict,self._modes[self._c_gtype])
            #now find the setting we want
            val=findindict(setdict,sd["settingname"])
            #now create the control           
            #label
            b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((550, 300), (200, 40)),
                 text="{}:{}".format(sd["title"],val),
                 manager=self._manager,
                 visible=True)  
            self._gamemode_settings["{}_label".format(sn)]=b
            #first work out what type it is
            if sd["type"]=="slider":
                #slide
                b=pygame_gui.elements.ui_horizontal_slider.UIHorizontalSlider(relative_rect=pygame.Rect((750, 300), (200, 40)),
                     start_value=val,
                     value_range=(sd["llimit"],sd["ulimit"]),
                     manager=self._manager,
                     visible=True)  
                self._set_buttons["{}_setter".format(sn)]=b

        
    def _randomise(self):
        """
        Pick a world and update settings accordingly
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.world=="Random":
            worldname=random.choice(list(settings["terrain"]["worlds"].keys()))
        else:
            worldname=self.world
        print(worldname)
        world=settings["terrain"]["worlds"][worldname]
        settings["terrain"].update(world)
        #settings["terrain"]["colour"]=random.choice(settings["terrain"]["colours"])
            
    def on_loop(self):
        """
        Items that have loop methods go here. Also looks for crashes, landings etc
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        pass
        
        
    def on_render(self):
        """
        Update screen. Calls all the group update methods and shows the screen
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        try:
            #fill the screen with black
            self._display_surf.fill((0,0,0))
            #we only render if there is a -1 in the state
            if self._guistate==-1:
                #set buttons ot visible
                for en,el in self._mm_buttons.items():
                    el.show()
                #set image
                self.image=self._mm_image
                #set game type
                self._mm_buttons["gmode"].set_text(self._c_gtype)
                self._guistate=-self._guistate
            elif self._guistate==-2:
                #set buttons to visible
                for en,el in self._set_buttons.items():
                    el.show()
                #set image
                self.image=self._set_image
                self._guistate=-self._guistate
            elif self._guistate==-3:
                #set buttons to visible
                for en,el in self._credits_buttons.items():
                    el.show()
                #set image
                self.image=self._credits_image
                self._guistate=-self._guistate
            elif self._guistate==-4:
                #set buttons to visible
                for en,el in self._highscores_buttons.items():
                    el.show()
                #set image
                self.image=self._highscores_image
                self._guistate=-self._guistate
                #now populate
                self.show_highscores()
            elif self._guistate==4:
                #highscores has things to render too
                self._highscores_table_draw()
            #and show the splash screen
            self._display_surf.blit(self.image, self.rect)
            #and draw ui
            self._manager.draw_ui(self._display_surf)
            #Refresh Screen
            pygame.display.flip()
        except pygame.error:
            #we have probably quit from within the game. exit
            print("Thank you for playing lander!")
            self._running=False
    
    def show_highscores(self,rebuild=True):
        """
        Show the high scores table
        
        INPUTS:
        rebuild: True if we are rebuilding names menu
        
        OUTPUTS:
        None
        """
        #first, get the selected world
        gtype=self._highscores_buttons["gametypes"].selected_option
        #names in dropdown
        if rebuild:
            names=self._highscores.names(gtype)
            names=["All"]+names
            b=pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(relative_rect=pygame.Rect((475, 100), (150, 40)),
                 options_list=names,
                 starting_option="All",
                 manager=self._manager,
                 visible=True)  
            #self._highscores_table_elements.append(b)
            self._highscores_selectedplayer=b
        #get the name
        cplayer=self._highscores_selectedplayer.selected_option
        #now get scores. 
        if cplayer=="All":
            scores=self._highscores.scores(gtype)
        else:
            scores=self._highscores.scores(gtype,name=cplayer)
        #now build the table
        if not scores:
            #no scores
            #label
            b=pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((20, 150), (200, 40)),
                 text="No scores!",
                 manager=self._manager,
                 visible=True)  
            self._highscores_table_elements.append(b)
            return
        #first get number of columns to divide screen up. table is 6/8 screen
        cols=scores[0].keys()
        twidth=self._width*6/8
        cwidth=twidth/len(cols)
        #now height
        sheight=150 #start height
        mxheight=20 #dist from bottom of screen
        scorerowheight,scorerowpad=25,2.5 #height of score row
        headingrowheight,headingrowpad=50,5 #height of heading row
        theight=self._height-sheight-mxheight #total table height. 
        theight=math.floor(theight/50)*50 #ensure its divisble by 50
        #and work out rows
        rows=int(
            min(
            [math.floor(theight/scorerowheight)-headingrowheight/scorerowheight,
            len(scores)])) #and how many text rows there are
        #now make the headings. these are text. consider making buttons for sorting later
        for cc,cn in enumerate(cols):
            #label
            b=pygame_gui.elements.ui_label.UILabel(
                relative_rect=pygame.Rect(
                    (cwidth*cc+self._width/8+5, sheight+headingrowpad), (cwidth-10, headingrowheight-headingrowpad)),
                text=cn,
                manager=self._manager,
                visible=True,
                object_id="highscoreheading")  
            self._highscores_table_elements.append(b)
            #now fill the data in
            for row in range(rows):
                #data
                val=scores[row][cn]
                if isinstance(val,numbers.Number):
                    val="{:5.0f}".format(val)
                elif type(val)==datetime.datetime:
                    #data time
                    val=val.strftime("%Y-%m-%d")
                #label
                if row==0:
                    obj="highscore1"
                elif row==1:
                    obj="highscore2" 
                elif row==2:
                    obj="highscore3" 
                elif (row % 2)==0:
                    obj="highscoreeven" 
                else:
                    obj="highscoreodd" 
                b=pygame_gui.elements.ui_label.UILabel(
                    relative_rect=pygame.Rect(
                        (cwidth*cc+self._width/8+5, 
                        row*scorerowheight+sheight+headingrowheight+scorerowpad), 
                        (cwidth-10, scorerowheight-scorerowpad)),
                    text=val,
                    manager=self._manager,
                    visible=True,
                    object_id=obj
                    )  
                self._highscores_table_elements.append(b)
            
        #now add number column
        for row in range(rows):
            #label
            #label
            if row==0:
                obj="highscore1"
            elif row==1:
                obj="highscore2" 
            elif row==2:
                obj="highscore3" 
            elif (row % 2)==0:
                obj="highscoreeven" 
            else:
                obj="highscoreodd" 
            b=pygame_gui.elements.ui_label.UILabel(
                relative_rect=pygame.Rect(
                    (self._width/8-25, sheight+headingrowheight+row*scorerowheight), 
                    (25, scorerowheight-scorerowpad)
                    ),
                text="{}".format(row+1),
                manager=self._manager,
                visible=True,
                object_id=obj)  
            self._highscores_table_elements.append(b)
        
        #now the grapchical elements
        def fcn(cols=cols,cw=cwidth,th=theight,sh=sheight,screen=self._display_surf):
            for i,cn in enumerate(cols):
                #a vertical column box
                r=pygame.draw.rect(
                    self._display_surf,0xff4040,
                    pygame.Rect(screen.get_width()/8+i*cw,sh,cw,th),
                    2
                    )
        self._highscores_table_draw=fcn
       
            
        
    
    def on_cleanup(self):
        """
        When we want to quit
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        pygame.quit()
 
    def on_execute(self):
        """
        Run the game. this contains the main loop
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            #events
            for event in pygame.event.get():
                self.on_event(event)
            #we can exit here if iteration has caused a quit
            if not self._running:
                break
            #loop, update() functions
            self.on_loop()
            #update/render
            self.on_render()
            #Number of frames per secong e.g. 60
            td=self._clock.tick(60)/1000
            self._manager.update(td)
        self.on_cleanup()
 
 
 
if __name__ == "__main__" :
    #init pygame
    pygame.init()
    pygame.font.init()
    #settings. these define difficulty levels etc
    #read in config file
    with open("settings.yaml") as file:
        settings = yaml.load(file, Loader=yaml.FullLoader) 
    lander = MainMenu(settings)
    lander.on_execute()

