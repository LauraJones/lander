"""
Package delivery: Pick package up at one landing pad and take to another

TODO:
 - Add HUD message/pause capability
 - Scoring
 - Starup message on HUD
 
"""


import pygame
from pygame.locals import *
import os.path
import cmath
import math
from scipy.stats import linregress
import random
import time
import pygame_gui
import json
import baseplayer
from pathlib import Path

class Rocket(baseplayer.baseplayer):
    """
    Package delivery class class is the player in he game
    draws a rocket, effected by gravity. Crashes if lands on terrain,
    """
    def __init__(self,options,settings,highscores,game):
        """
        Initalisation. saves parameters and sets initial ship state
        
        INPUTS:
        options: options (things like which image sprite to use, gravity, etc)
        settings: settings
        highscores: highscores object
        game: game object
        """
        super().__init__(options,settings,highscores,game)
        #save options
        #highscores
        self._highscores=highscores
        #and set up table
        self._highscores.creategametype(
            "package",
            json.dumps(["name","score","numpackages","datetime","crash"]),
            json.dumps({"numpackages":"# packages","crash":"Crashed","datetime":"Date"})
            )
        #scoring factors
        self._scoringfactors=options["scoring"]
        #number of successful lands
        self.numlands=0
        #and initial time
        self.remainingtime=options["initialtime"]
        self._timeperpackage=options["timeperland"] #goes down by 1s per delivery
        self._lasttime=time.time()
        #and target pad sprite
        self._targetsprite=None
        #and package images
        self._packageimages=options["packages"]
        
    def on_init(self,screen,x,y,manager):
        """
        Set up graphics, read image files etc
        
        INPUTS:
        screen: The screen to draw on
        x: Starting x location
        y: Starting y location
        manager: GUI manager for window
        
        OUTPUTS: 
        None
        """
        #init superclass
        super().on_init(screen,x,y,manager)
        #now bases (list of images)
        bases=self._terrainoptions["baseimages"]
        self._baseimgs=[]
        for base in bases:
            #path
            fpath=os.path.join("images",base)
            #load file
            self._baseimgs.append(pygame.image.load(fpath).convert_alpha())
        #show welcome
        self.showwelcome()
        
        
    def addlandingpads(self,terrain):
        """
        Add landing pads and related scenery to map
        Called immediately after hightmap is made
        
        INPUTS:
        terrain: terrain object
        
        OUTPUTS:
        heightmap and list of sprites
        """
        #save terrian object
        self._terrain=terrain
        hm=terrain.getheightmap()
        #now add the pads. We add the amounts requested.
        #they are equally spaced. Essentially we break the screen up into N sections and put a pad on each one
        self._pads=[]
        self._bases=[]
        for seg in range(self._terrainoptions["numpads"]):
            #work out bounds
            minx=seg/self._terrainoptions["numpads"]*len(hm)
            maxx=(seg+1)/self._terrainoptions["numpads"]*len(hm)
            #add pads
            pad,base=self.addlandingpad(terrain,int(minx),int(maxx))
            #add to lists
            self._pads.append(pad)
            if not base is None:
                self._bases.append(base)
        #set the target
        self.settarget()
        #return
        return self._pads, self._bases
        
        
    
    def addlandingpad(self,terrain,minx,maxx):
        """
        Add a single landing pad and related scenery to map
        
        INPUTS:
        terrain: terrain object
        minx: The minimum x to look in
        maxx: The maximum X to look in
        
        OUTPUTS:
        heightmap and list of sprites
        """
        #landing pad is placed at the flattest spot on the map
        padwidth=self._terrainoptions["padwidth"]
        #first, create a landing pad
        landingpad_o=baseplayer.landingpad(3,padwidth,[0,255,0])
        #now find the best spot
        best,selector=terrain.findarea(padwidth,landingpad_o.suitabilitychecker,minx,maxx)
        #lets not worry about None here, as there should be somwhwere
        #and place
        height=terrain.createfoundation(best,padwidth)
        landingpad_o.on_init(self._screen,best,height)
        #and save for later
        self._landingpad=landingpad_o
        
        #now the base this is located near the pad (within 4 pad sizes)
        #first, randomly select a pad
        baseimg=random.choice(self._baseimgs)
        basewidth=baseimg.get_width()
        #and the credible areas
        base_o=baseplayer.base(baseimg)
        basex,leftselector=terrain.findarea(basewidth,base_o.suitabilitychecker,best-basewidth*3,best+padwidth+basewidth*3)
        if not basex is None:
            #was n area, place
            #flatten area
            basey=terrain.createfoundation(basex,basewidth)
            #and place the base
            base_o.on_init(self._screen,basex,basey)       
            #and return
            return landingpad_o,base_o
        else:
            #no spot to put base, so just no base
            return landingpad_o,None
            
            
    def settarget(self):
        """
        Set a new target for delivery
        Delivery target is a random landing pad that is not one we are currently on
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #remove any existing packages
        if not self._targetsprite is None:
            self._targetsprite.kill()
            self._targetsprite=None
        #build a list of pads and how far away we are in X to their centres
        candidates=[]
        for pad in self._pads:
            if not self.rect.colliderect(pad.rect):
                #is a candidate
                candidates.append(pad)
        #now pick one
        dpad=random.choice(candidates)
        #we need to set this one as active and the reset as inactive
        for pad in self._pads:
            if pad.terrainx==dpad.terrainx:
                #is active
                pad.destination=True
            else:
                pad.destination=False
        #now get its location and put a package on it
        #it always goes on the left edge, so X is consistent
        #pick a package
        self._targetsprite=package(random.choice(self._packageimages))
        self._targetsprite.on_init(self._screen,dpad.terrainx,dpad.terrainy)
         
        
            
    def loop(self):
        """
        Loop. call main class and then check for land/crash
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        super().loop()
        self.landingpads()
        self.terrain()
        #and timer
        self.time()
    
    def update(self,screenleft):
        """
        Update method. blits ourselves onto the screen
        
        INPUTS:
        screenleft: The left side of the screen
        
        OUTPUTS: 
        None
        """
        if not self._targetsprite is None:
            self._targetsprite.update(screenleft)
        super().update(screenleft)
        
    def time(self):
        """
        Calculate timer and victory conditions
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if not (self.paused or self.crashed):
            ct=time.time()
            self.remainingtime-=(ct-self._lasttime)
            self._lasttime=ct
            if self.remainingtime<=0:
                #say we crashed so that time stops, show win screen
                self.crashed=True
                #calc score and show
                self.score,self._scoredict=self.calculatescore(False)
                self._dlg=dialogue(
                    not self.crashed,self.score,self._scoredict,self._highscores,self._screen,self._manager,self.exit,"package"
                    )
                
        elif self.paused:
            #ensure time slips
            self._lasttime=time.time()
    
    def calculatescore(self,crashed=True):
        """
        Calculate the score. Score is based on points per package.
        Scores are altered by a number of factors:
         - wheter player ended by crashing or running out of time
         - gravity
         - autostraighten
         - power/weight ratio
        
        INPUTS:
        crashed: True if the game ended through crashing
        
        OUTPUTS:
        score, scoring parameters
        """         
        #calculate scores
        #first, base score
        score=self.numlands*self._scoringfactors["package"]
        #now crashed multiplier
        if crashed:
            score=score*self._scoringfactors["crashmultiplier"]
        
        #now the factors
        #first autostraighten. this is simply boolean 
        sf=1
        if self._autostraighten:
            sf=self._scoringfactors["straighten"]
            score=score*sf
        #now for gravity. Gravity centres on 1 at centregravity
        #on the high end it reaches highgravityval at higgravity
        #on the low end it reaches lowgravityval at lowgravity
        if self._gravity>self._scoringfactors["centregravity"]:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["highgravity"]-self._scoringfactors["centregravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["highgravityval"]-1)+1
        else:
            #work out how many high gravity ratios we are out
            ratios=(self._gravity-self._scoringfactors["centregravity"])/(self._scoringfactors["centregravity"]-self._scoringfactors["lowgravity"])
            #now calc fcator
            gfactor=ratios*(self._scoringfactors["lowgravityval"]-1)+1
        score=score*gfactor
        #now power/weight ratio
        #higher is obviously easier. We make it linear between 1 and 3, where 1 is powerweightmult and 5 is 1
        pwratio=self._thrusterforce/(self._mass*self._gravity)
        slope=-(self._scoringfactors["powerweightmult"]-1)/2
        pwratiofactor=max(
            self._scoringfactors["minpowerweightmult"],
            self._scoringfactors["powerweightmult"]+pwratio*slope
            )
        score=score*pwratiofactor
        #now show the message
        params=dict(
            numpackages=self.numlands,
            crash=crashed,
            straighen=self._autostraighten,
            gravity=self._gravity,
            powerweight=pwratio,
            straightenfactor=sf,
            gravityfactor=gfactor,
            pwratiofactor=pwratiofactor
            )
        return score,params
    
    def landingpads(self):
        """
        Assess landing pads to see if we have landed
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        if self.crashed:
            return
        for pad in self._pads:
            if self.rect.colliderect(pad.rect):
                #landed/crashed
                if self.safeland():
                    self.land(pad)
                else:
                    self.angle=0
                    self.crash()
                return
        #not landed
        self.landed=False
        
    def land(self,pad):
        """
        Land on a landing pad. We set the next objective 
        
        INPUTS:
        pad: the landing pad
        
        OUTPUTS:
        None
        """
        if self.landed:
            return
        #set landed flag
        self.landed=True
        #and set velocity, angle to zero
        self._vx=0
        self._vy=0
        self.angle=0
        
        #and if the pad is not a target, return
        if not pad.destination:
            return
        
        #add 50% fuel
        self.fuel=int(min([self.fuel+0.5*self.initfuel,self.initfuel]))
        #and percent
        self.fuelpc=self.fuel/self.initfuel
        
        #and increment landed
        self.numlands+=1
        
        #set new target (also removes package sprite)
        self.settarget()
        
        #time
        self.remainingtime+=self._timeperpackage
        self._timeperpackage=max([self._timeperpackage-1,0])
        
        #and a message to say how we went
        if self.numlands==1:
            self.showmessage("{} package delivered!".format(self.numlands),5)
        else:
            self.showmessage("{} packages delivered!".format(self.numlands),5) 
        
        
    def crash(self):
        """
        Crash the ship. Sessentially jst extends base to enable scoring
        
        INPUTS:
        None
        
        OUTPUTS:
        None
        """
        #super
        super().crash()
        #now score
        self.score,self._scoredict=self.calculatescore(True)
        self._dlg=dialogue(
            not self.crashed,self.score,self._scoredict,self._highscores,self._screen,self._manager,self.exit,"package"
            )
        
        
        
        
    def safeland(self):
        """
        Determine if a landing is safe or should result in a crash
        
        INPUTS:
        None
        
        OUTPUTS:
        bool (true for safe landing)
        """
        return math.sqrt(self._vx**2+self._vy**2)<=self._maxlandingvelocity
        
        
class dialogue(baseplayer.dialogue):
    """
    Class to show a dialogue box when crashed/landed
    """
    def __init__(self,landed,score,stats,highscores,screen,manager,cbmenu,mode):
        """
        Initialise
        
        INPUTS:
        landed: bool: did we land (true) or crash (false)
        score: Final score
        stats: stats dict. passed to highscores but also some properties are displayed
        highscores: High scores object
        screen: pygame screen object
        manager: GUI manager (pygame_gui)
        cbmenu: Callback for main menu button press
        mode: Game mode
        
        OUTPUTS:
        None
        """
        #save params
        self._landed=True #so save works
        self._score=score
        self._stats=stats
        self._highscores=highscores
        self._screen=screen
        self._manager=manager
        self._cbmenu=cbmenu
        self._mode=mode
        #work out the rank of our score
        rank,total=self._highscores.rank(self._mode,self._score)
        #create text strings
        if stats["numpackages"]==1:
            text="Good Job! you delivered {} package<br>and scored {:.0f} points!".format(stats["numpackages"],score)
        else:
            text="Good Job! you delivered {} packages<br>and scored {:.0f} points!".format(stats["numpackages"],score)
        if landed:
            title="Not a scratch on 'er!"
        else:
            #title of box
            title="Too bad about the scratches"
            #text in box. 
            text+="...With a minor hiccup at the end"
        #draw ourselves on the screen
        #create rect
        self._mbrect=pygame.Rect(
                0,0,
                400,250)
        #centre it on the screen
        self._mbrect.center=(self._screen.get_width()/2,self._screen.get_height()/2)
        #and the window
        self._mbwindow=pygame_gui.elements.UIWindow(self._mbrect,
            manager=self._manager,
            window_display_title=title
            )
        #OK button closes the dialogue
        self._okbutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(280,150,80,40),
            text="OK",
            manager=self._manager,
            container=self._mbwindow
            )
        #Menu button goes ot main menu
        self._menubutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(180,150,80,40),
            text="Menu",
            manager=self._manager,
            container=self._mbwindow
            )
        #now text
        self._boxtxt=pygame_gui.elements.ui_text_box.UITextBox(
            html_text=text,
            relative_rect=pygame.Rect(0,0,368,80),
            manager=self._manager,
            container=self._mbwindow
            )
        #name entry box
        #heading
        self._savelabel=pygame_gui.elements.ui_label.UILabel(
            relative_rect=pygame.Rect(0,80,200,30),
            text="Save your score!",
            manager=self._manager,
            container=self._mbwindow
            ) 
        #lable for text box
        self._namelabel=pygame_gui.elements.ui_label.UILabel(
            relative_rect=pygame.Rect(0,110,80,30),
            text="Name",
            manager=self._manager,
            container=self._mbwindow
            )  
        #text box
        self._nameentry=pygame_gui.elements.ui_text_entry_line.UITextEntryLine(
            relative_rect=pygame.Rect(80,110,150,30),
            manager=self._manager,
            container=self._mbwindow
            )
        #save score button
        self._savebutton=pygame_gui.elements.ui_button.UIButton(
            relative_rect=pygame.Rect(230,110,80,30),
            text="Save",
            manager=self._manager,
            container=self._mbwindow
            )    
        

                    
                    
class package(pygame.sprite.Sprite):
    """
    This is a package image to display where the base is
    """
    def __init__(self,image):
        """
        Initialisation
        
        INPUTS:
        image: image surface to display
        """
        super().__init__()
        #load the image
        fpath=os.path.join("images",str(Path(image)))
        #load file
        self.image=pygame.image.load(fpath).convert_alpha()
        
        
    def on_init(self,screen,x,y):
        """
        Set up graphics,
        
        INPUTS:
        screen: The screen to draw on
        x: our x location
        y: our y location (height form bottom)
        
        OUTPUTS: 
        None
        """
        #now save settings
        self.terrainx=x
        self.terrainy=y
        self._screen=screen
        self.screenheight=screen.get_height()
        self.screenwidth=screen.get_width()
        #and our rect
        self.rect=self.image.get_rect()
        self.rect.bottomleft=(self.terrainx,self.screenheight-self.terrainy)
        #and save width
        self.width=self.rect.width
        
    def update(self,screenleft):
        """
        Draw ourselves
        
        INPUTS:
        screenleft: Left side of screen
        
        OUTPUTS:
        None
        """
        self.rect.x=self.terrainx-screenleft
        if (self.terrainx+self.width)>=screenleft and self.terrainx<=(screenleft+self.screenwidth):
            self._screen.blit(self.image, self.rect)  